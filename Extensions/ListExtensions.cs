﻿using System;
using System.Collections.Generic;
using System.Linq;
using WarLightStarterBot.Moves;

namespace WarLightStarterBot.Extensions
{
    public static class ListExtensions
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            var rng = new Random();
            var n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void AddAttack(this IList<AttackTransferMove> list, AttackTransferMove element)
        {
            foreach (var item in list)
            {
                if (list.Any(l => l.FromRegion == element.FromRegion && l.ToRegion == element.ToRegion))
                {
                    var attackMove = list.First(l => l.FromRegion == item.FromRegion);
                    attackMove.Armies += element.Armies;
                    return;
                }
            }

            list.Add(element);
        }
    }
}
