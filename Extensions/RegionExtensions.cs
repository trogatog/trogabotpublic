﻿using System.Collections.Generic;
using System.Linq;
using WarLightStarterBot.Bot;
using WarLightStarterBot.Main;
using WarLightStarterBot.Moves;

namespace WarLightStarterBot.Extensions
{
    public static class RegionExtensions
    {
        public static void MakeMeInvisible(this Region region, bool force = false)
        {
            if(!region.StrategicPosition || force)
                foreach (var neighbor in region.Neighbors)
                    neighbor.Neighbors.Remove(region);
        }

        public static bool HasAttacked(this Region region, MyTurnMoves myMoves)
        {
            if (myMoves.AttackTransferMoves.Any(attack => attack.FromRegion == region) || myMoves.LastAttackMoves.Any(attack => attack.FromRegion == region))
            {
                return true;
            }
            return false;
        }

        public static List<Region> WasIAttacked(this Region region, BotState gameState)
        {
            var attackers = new List<Region>();
            if(gameState.RoundNumber == 1)
                return attackers;

            int lastRound = gameState.RoundNumber - 1;
            var opponentsLastMoves = gameState.Opponent.AttackTransferMovesForRound[lastRound];
            attackers.AddRange(from attack in opponentsLastMoves where attack.ToRegion.Id == region.Id select attack.FromRegion);

            return attackers;
        }
    }
}
