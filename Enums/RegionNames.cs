﻿using System;

namespace WarLightStarterBot.Enums
{
    //1=highest 5=lowest
    public class SubRegionsRanking : Attribute
    {
        internal SubRegionsRanking(int rank)
        {
            SubRegionRank = rank;
        }
        public int SubRegionRank { get; private set; }
    }
    
    //1=highest 5=lowest
    public class RankingAttribute : Attribute
    {
        internal RankingAttribute(int rank)
        {
            Rank = rank;
        }
        public int Rank { get; private set; }
    }

    //1=highest 5=lowest
    public class PickRankAttribute : Attribute
    {
        internal PickRankAttribute(int rank)
        {
            PickRank = rank;
        }
        public int PickRank { get; private set; }
    }

    public class StrategicRegion : Attribute
    {
        internal StrategicRegion(bool isStrategic)
        {
            IsStrategicRegion = isStrategic;
        }
        public bool IsStrategicRegion { get; private set; }
    }

    public enum SuperRegionNames
    {
        [Ranking(3)]
        [PickRank(3)]
        Africa = 4,

        [Ranking(6)]
        [PickRank(6)]
        Asia = 5,

        [Ranking(2)]
        [PickRank(1)]
        Australia = 6,

        [Ranking(4)]
        [PickRank(4)]
        Europe = 3,

        [Ranking(5)]
        [PickRank(5)]
        NorthAmerica = 1,

        [Ranking(1)]
        [PickRank(2)]
        SouthAmerica = 2
    }

    public enum RegionNames
    {
        #region North America
        [SubRegionsRanking(5)]
        [Ranking(1)]
        [StrategicRegion(true)]
        Alaska = 1,

        [SubRegionsRanking(5)]
        [Ranking(5)]
        [StrategicRegion(false)]
        NorthwestTerritory = 2,

        [SubRegionsRanking(5)]
        [Ranking(1)]
        [StrategicRegion(true)]
        Greenland = 3,

        [SubRegionsRanking(5)]
        [Ranking(5)]
        [StrategicRegion(false)]
        Alberta = 4,

        [SubRegionsRanking(4)]
        [Ranking(4)]
        [StrategicRegion(false)]
        Ontario = 5,

        [SubRegionsRanking(5)]
        [Ranking(3)]
        [StrategicRegion(false)]
        Quebec = 6,

        [SubRegionsRanking(5)]
        [Ranking(5)]
        [StrategicRegion(false)]
        WesternUnitedStates = 7,

        [SubRegionsRanking(5)]
        [Ranking(4)]
        [StrategicRegion(false)]
        EasternUnitedStates = 8,

        [SubRegionsRanking(5)]
        [Ranking(3)]
        [StrategicRegion(true)]
        CentralAmerica = 9,
        #endregion

        #region South America
        [SubRegionsRanking(5)]
        [Ranking(2)]
        [StrategicRegion(true)]
        Venezuela = 10,

        [SubRegionsRanking(3)]
        [Ranking(4)]
        [StrategicRegion(false)]
        Peru = 11,

        [SubRegionsRanking(2)]
        [Ranking(1)]
        [StrategicRegion(true)]
        Brazil = 12,

        [SubRegionsRanking(6)]
        [Ranking(5)]
        [StrategicRegion(false)]
        Argentina = 13,
        #endregion

        #region Europe
        [SubRegionsRanking(3)]
        [Ranking(4)]
        [StrategicRegion(true)]
        Iceland = 14,

        [SubRegionsRanking(4)]
        [Ranking(3)]
        [StrategicRegion(false)]
        GreatBritain = 15,

        [SubRegionsRanking(4)]
        [Ranking(3)]
        [StrategicRegion(false)]
        Scandinavia = 16,

        [SubRegionsRanking(5)]
        [Ranking(5)]
        [StrategicRegion(true)]
        Ukraine = 17,

        [SubRegionsRanking(4)]
        [Ranking(2)]
        [StrategicRegion(true)]
        WesternEurope = 18,

        [SubRegionsRanking(6)]
        [Ranking(3)]
        [StrategicRegion(false)]
        NorthernEurope = 19,

        [SubRegionsRanking(4)]
        [Ranking(1)]
        [StrategicRegion(true)]
        SouthernEurope = 20,
        #endregion

        #region Africa
        [SubRegionsRanking(2)]
        [Ranking(2)]
        [StrategicRegion(true)]
        NorthAfrica = 21,

        [SubRegionsRanking(4)]
        [Ranking(3)]
        [StrategicRegion(true)]
        Egypt = 22,

        [SubRegionsRanking(3)]
        [Ranking(4)]
        [StrategicRegion(true)]
        EastAfrica = 23,

        [SubRegionsRanking(5)]
        [Ranking(4)]
        [StrategicRegion(false)]
        Congo = 24,

        [SubRegionsRanking(6)]
        [Ranking(5)]
        [StrategicRegion(false)]
        SouthAfrica = 25,

        [SubRegionsRanking(7)]
        [Ranking(5)]
        [StrategicRegion(false)]
        Madagascar = 26,
        #endregion

        #region Asia
        [SubRegionsRanking(8)]
        [Ranking(5)]
        [StrategicRegion(true)]
        Ural = 27,

        [SubRegionsRanking(8)]
        [Ranking(4)]
        [StrategicRegion(false)]
        Siberia = 28,

        [SubRegionsRanking(8)]
        [Ranking(2)]
        [StrategicRegion(false)]
        Yakutsk = 29,

        [SubRegionsRanking(8)]
        [Ranking(1)]
        [StrategicRegion(true)]
        Kamchatka = 30,

        [SubRegionsRanking(8)]
        [Ranking(4)]
        [StrategicRegion(false)]
        Irkutsk = 31,

        [SubRegionsRanking(8)]
        [Ranking(3)]
        [StrategicRegion(true)]
        Kazakhstan = 32,

        [SubRegionsRanking(8)]
        [Ranking(3)]
        [StrategicRegion(false)]
        China = 33,

        [SubRegionsRanking(8)]
        [Ranking(4)]
        [StrategicRegion(false)]
        Mongolia = 34,

        [SubRegionsRanking(8)]
        [Ranking(5)]
        [StrategicRegion(false)]
        Japan = 35,

        [SubRegionsRanking(5)]
        [Ranking(1)]
        [StrategicRegion(true)]
        MiddleEast = 36,

        [SubRegionsRanking(6)]
        [Ranking(3)]
        [StrategicRegion(false)]
        India = 37,

        [SubRegionsRanking(7)]
        [Ranking(2)]
        [StrategicRegion(true)]
        Siam = 38,
        #endregion

        #region Australia
        [SubRegionsRanking(2)]
        [Ranking(3)]
        [StrategicRegion(true)]
        Indonesia = 39,

        [SubRegionsRanking(3)]
        [Ranking(4)]
        [StrategicRegion(false)]
        NewGuinea = 40,

        [SubRegionsRanking(3)]
        [Ranking(5)]
        [StrategicRegion(false)]
        WesternAustralia = 41,

        [SubRegionsRanking(4)]
        [Ranking(5)]
        [StrategicRegion(false)]
        EasternAustralia = 42
        #endregion
    }
}
