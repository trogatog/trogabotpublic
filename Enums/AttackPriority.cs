﻿namespace WarLightStarterBot.Enums
{
    public enum AttackPriority
    {
        PriorityAttack,
        FirstTransfer,
        NeutralAttack,
        OpponentAttack
    }
}
