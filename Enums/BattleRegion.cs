﻿namespace WarLightStarterBot.Enums
{
    public enum BattleRegions
    {
        Attacker,
        Defender,
        Neither
    }
}
