using System;
using WarLightStarterBot.Main;

namespace WarLightStarterBot.Moves
{

    /**
     * This Move is used in the second part of each round. It represents the attack or transfer of armies from
     * fromRegion to toRegion. If toRegion is owned by the player himself, it's a transfer. If toRegion is
     * owned by the opponent, this Move is an attack. 
     */

    public class AttackTransferMove : Move
    {
        public AttackTransferMove(string playerName, Region fromRegion, int toRegion, int armies)
            : this(playerName, fromRegion, new Region(toRegion), armies)
        {

        }

        public AttackTransferMove(string playerName, Region fromRegion, Region toRegion, int armies)
            : base(playerName)
        {
            FromRegion = fromRegion;
            ToRegion = toRegion;
            Armies = armies;
        }

        public int Armies { get; set; }

        public Region FromRegion { get; private set; }

        public Region ToRegion { get; private set; }

        public string String
        {
            get
            {
                try
                {
                    if (string.IsNullOrEmpty(IllegalMove))
                        return PlayerName + " attack/transfer " + FromRegion.Id + " " + ToRegion.Id + " " + Armies;
                }
                catch
                {
                    Console.Error.Write("Error submitting string...  WTH?");
                    return null;
                }
                return PlayerName + " illegal_move " + IllegalMove;
            }
        }

    }
}