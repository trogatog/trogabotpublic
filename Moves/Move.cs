namespace WarLightStarterBot.Moves
{

    public class Move
    {
        public Move(string playerName)
        {
            PlayerName = playerName;
            IllegalMove = "";
        }

        public string PlayerName { get; set; }

        public string IllegalMove { get; set; }
    }
}