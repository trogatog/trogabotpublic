﻿using System.Collections.Generic;

namespace WarLightStarterBot.Moves
{
    public class MyTurnMoves
    {
        public MyTurnMoves()
        {
            FirstTransferMoves = new List<AttackTransferMove>();
            AttackTransferMoves = new List<AttackTransferMove>();
            LastAttackMoves = new List<AttackTransferMove>();
        }

        public List<AttackTransferMove> FirstTransferMoves { get; set; }
        public List<AttackTransferMove> AttackTransferMoves { get; set; }
        public List<AttackTransferMove> LastAttackMoves { get; set; }
    }
}
