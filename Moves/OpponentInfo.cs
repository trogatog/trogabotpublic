﻿using System;
using System.Collections.Generic;
using System.Linq;
using WarLightStarterBot.Bot;
using WarLightStarterBot.Enums;
using WarLightStarterBot.Main;
using WarLightStarterBot.ServerCommands;

namespace WarLightStarterBot.Moves
{
    public class OpponentInfo
    {
        public List<Region> Regions { get; set; }
        public List<Region> StartingRegions { get; private set; }
        public Map KnownOwnedRegions { get; private set; }

        public string PlayerName { get; private set; }
        public List<SuperRegion> OwnedSuperRegions { get; private set; }
        public int VisibleIncome { get; private set; }
        public Dictionary<int, List<AttackTransferMove>> AttackTransferMovesForRound { get; private set; }
        public Dictionary<int, Dictionary<Region, int>> PlaceArmyMovesForRound { get; private set; }
        public bool SinglePlaceArmyThisRound { get; private set; }

        public OpponentInfo(string opponentName)
        {
            OwnedSuperRegions = new List<SuperRegion>();
            PlayerName = opponentName;
            AttackTransferMovesForRound = new Dictionary<int, List<AttackTransferMove>>();
            PlaceArmyMovesForRound = new Dictionary<int, Dictionary<Region, int>>();
            KnownOwnedRegions = new Map();
            StartingRegions = new List<Region>();
        }

        public void SetPlayerMovesForRound(BotState state, List<PlaceArmiesMove> placeArmiesMoves, List<AttackTransferMove> attackTransferMoves)
        {
            if (state.RoundNumber == 0)
                return;

            var moves = new Dictionary<Region, int>();
            foreach (var move in placeArmiesMoves)
            {
               if(!moves.ContainsKey(move.Region)) 
                   moves.Add(move.Region, 0);

                moves[move.Region] += move.Armies;
            }

            PlaceArmyMovesForRound.Add(state.RoundNumber, moves);
            SinglePlaceArmyThisRound = moves.Count == 1;

            if (OwnedSuperRegions.Count == 0 && state.RoundNumber > 0)
            {
                foreach (var super in KnownOwnedRegions.Regions.Select(r => r.SuperRegion).OrderBy(s => s.Ranking).Distinct().ToList())
                {
                    var superRegion = state.FullMap.GetSuperRegion(super.Id);
                    var unusedArmies = 0;
                    foreach (var move in PlaceArmyMovesForRound[state.RoundNumber])
                    {
                        var region = move.Key.Id;
                        if (state.FullMap.GetRegion(region).SuperRegion.Id != super.Id)
                        {
                            unusedArmies += move.Value;
                        }

                    }
                    var currentEstimatedIncome = EstimatedIncome;
                    var armiesUsedForCapture = currentEstimatedIncome - unusedArmies;

                    if(super.RoundsUntilOpponentControlled < 100)
                        superRegion.ArmiesNeededToConquer -= armiesUsedForCapture;
                    else
                        superRegion.SetArmiesNeededToConquer();
                    if (superRegion.ArmiesNeededToConquer > 0)
                        superRegion.RoundsUntilOpponentControlled++;
                    else //ArmiesNeededToConquer <= 0
                    {
                        //only calculate against the first super region so as not to start removing extra supers
                        superRegion.RoundsUntilOpponentControlled = 0;
                        break;
                    }
                    
                }
            }
            foreach (var super in KnownOwnedRegions.Regions.Select(r => r.SuperRegion.Id).Distinct().ToList())
            {
                var superRegion = state.FullMap.GetSuperRegion(super);
                
                //super region can be -1 to offset me adding to it.
                superRegion.RoundsUntilOpponentControlled--;
                
                if (superRegion.RoundsUntilOpponentControlled <= 0)
                {
                    AddOwnedSuperRegion(super, state.FullMap, state.RoundNumber);
                }
            }
            //keep track of what opponent owns
            foreach (var visibleRegion in Regions.Where(visibleRegion => KnownOwnedRegions.Regions.All(r => r.Id != visibleRegion.Id)))
            {
                KnownOwnedRegions.Regions.Add(visibleRegion);
            }

            //remove any region that I've gained from opponent
            foreach (var region in KnownOwnedRegions.Regions.Select(r => r.Id).Distinct().ToList())
            {
                var mapRegion = state.VisibleMap.GetRegion(region);
                if (mapRegion != null && (mapRegion.OwnedByPlayer(state.MyPlayerName) || mapRegion.OwnedByPlayer(Constants.NEUTRAL_PLAYER)))
                {
                    KnownOwnedRegions.Regions.Remove(KnownOwnedRegions.Regions.First(r => r.Id == region));
                    if (OwnedSuperRegions.Any(sr => sr.Id == mapRegion.SuperRegion.Id))
                    {
                        OwnedSuperRegions.Remove(OwnedSuperRegions.First(sr => sr.Id == mapRegion.SuperRegion.Id));
                        state.FullMap.GetSuperRegion(mapRegion.SuperRegion.Id).RoundsUntilOpponentControlled = 101;
                    }
                }
            }
            
            AttackTransferMovesForRound.Add(state.RoundNumber, attackTransferMoves);
            VisibleIncome = placeArmiesMoves.Sum(pam => pam.Armies);
            if (VisibleIncome == EstimatedIncome)
            {
                foreach (var super in KnownOwnedRegions.Regions.Select(r => r.SuperRegion).Distinct())
                {
                    super.RoundsUntilOpponentControlled++;
                }
            }
            if (VisibleIncome > 5)
            {
                GuessOpponentsIncome(state);
            }
        }

        public void UpdateMapInfo(BotState state)
        {
            if (state.RoundNumber == 0)
            {
                var myReceivedPicks = 0;
                //determine what regions opponent owns based on initial picks
                foreach (var startingPick in state.PreferredStartingRegions.Select(psr => psr.Id))
                {
                    if (myReceivedPicks == 3)
                        break;
                    if (state.VisibleMap.GetRegion(startingPick) != null &&
                        state.VisibleMap.GetRegion(startingPick).OwnedByPlayer(state.MyPlayerName))
                    {
                        myReceivedPicks++;
                    }
                    else
                    {
                        KnownOwnedRegions.Add(state.FullMap.GetRegion(startingPick));
                        StartingRegions.Add(state.FullMap.GetRegion(startingPick));
                        Console.Error.WriteLine("Opponent start position: {0} -- {1}",state.FullMap.GetRegion(startingPick).Name, startingPick);
                    }
                }
                var superArmyCount = new Dictionary<SuperRegion, int>();
                foreach (var ownedRegion in KnownOwnedRegions.Regions)
                {
                    if(!superArmyCount.ContainsKey(ownedRegion.SuperRegion))
                        superArmyCount.Add(ownedRegion.SuperRegion, 0);

                    superArmyCount[ownedRegion.SuperRegion]++;
                }
                if (superArmyCount.Any(s => s.Value == 2))
                {
                    var superRegions = superArmyCount.Where(s => s.Value == 2);
                    foreach (var super in superRegions)
                    {
                        var superRegion = state.FullMap.GetSuperRegion(super.Key.Id);
                        superRegion.RoundsUntilOpponentControlled = 1; 
                        superRegion.ArmiesNeededToConquer = (superRegion.SubRegions.Count - 2)*3 - 1;
                    }
                }
                if (KnownOwnedRegions.Regions.Count < 3)
                {
                    //let's best guess their picks
                    if (KnownOwnedRegions.Regions.Count <= 1)
                    {
                        //probably starting in Europe if I didn't
                        if (state.MyRegions.All(my => my.SuperRegion.Id != (int) SuperRegionNames.Europe))
                        {
                            foreach (var region in state.PickableStartingRegions.Where(r => r.SuperRegion.Id == (int) SuperRegionNames.Europe))
                            {
                                KnownOwnedRegions.Add(region);
                            }
                        }
                    }
                    if (KnownOwnedRegions.Regions.Count == 2)
                    {
                        foreach (var region in state.PickableStartingRegions.Where(r => r.SuperRegion.Id == (int) SuperRegionNames.Africa).OrderBy(r => r.SubRegionRanking))
                        {
                            if (region.Neighbors.All(n => !n.OwnedByPlayer(state.MyPlayerName)))
                            {
                                KnownOwnedRegions.Add(region);
                            }
                        }
                    }
                }
            }
        }

        public int EstimatedIncome
        {
            get { return 5 + OwnedSuperRegions.Sum(sr => sr.ArmiesReward); }
        }

        private void GuessOpponentsIncome(BotState state)
        {
            foreach (var unknownSuper in state.UnknownStatusSuper)
            {
                switch (unknownSuper.Id)
                {
                    case (int)SuperRegionNames.Africa:
                        if (VisibleIncome > 7)
                            AddOwnedSuperRegion(unknownSuper.Id, state.FullMap, state.RoundNumber);
                        break;

                    case (int)SuperRegionNames.Asia:
                        if (VisibleIncome > 14)
                            AddOwnedSuperRegion(unknownSuper.Id, state.FullMap, state.RoundNumber);
                        break;

                    case (int)SuperRegionNames.Australia:
                        AddOwnedSuperRegion(unknownSuper.Id, state.FullMap, state.RoundNumber);
                        break;

                    case (int)SuperRegionNames.Europe:
                        if (VisibleIncome > 9)
                            AddOwnedSuperRegion(unknownSuper.Id, state.FullMap, state.RoundNumber);
                        break;

                    case (int)SuperRegionNames.NorthAmerica:
                        if (VisibleIncome > 10)
                            AddOwnedSuperRegion(unknownSuper.Id, state.FullMap, state.RoundNumber);
                        break;

                    case (int)SuperRegionNames.SouthAmerica:
                        AddOwnedSuperRegion(unknownSuper.Id, state.FullMap, state.RoundNumber);
                        break;
                }
            }
        }

        private void AddOwnedSuperRegion(int superRegionId, Map copyFromMap, int roundNumber)
        {
            var fullMapSuperRegion = copyFromMap.GetSuperRegion(superRegionId);
            if (!OwnedSuperRegions.Contains(fullMapSuperRegion))
            {
                OwnedSuperRegions.Add(fullMapSuperRegion);
                foreach (var region in fullMapSuperRegion.SubRegions)
                {
                    region.PlayerName = PlayerName;
                    if (!KnownOwnedRegions.Regions.Contains(region))
                    {
                        KnownOwnedRegions.Add(region);
                    }
                }
                Console.Error.WriteLine("Round {1}: Opponent controls {0}", fullMapSuperRegion.Name, roundNumber);
            }
        }
    }
}
