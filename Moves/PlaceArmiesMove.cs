using System;
using WarLightStarterBot.Main;

namespace WarLightStarterBot.Moves
{

    /**
     * This Move is used in the first part of each round. It represents what Region is increased
     * with how many armies.
     */

    public class PlaceArmiesMove : Move
    {
        public PlaceArmiesMove(string playerName, Region region, int armies)
            : base(playerName)
        {
            Region = region;
            Armies = armies;
        }

        public int Armies { get; set; }

        public Region Region { get; private set; }

        public String String
        {
            get
            {
                if (string.IsNullOrEmpty(IllegalMove))
                    return PlayerName + " place_armies " + Region.Id + " " + Armies;

                return PlayerName + " illegal_move " + IllegalMove;
            }
        }
    }
}