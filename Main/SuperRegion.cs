using System.Collections.Generic;
using System.Linq;
using WarLightStarterBot.Enums;
using WarLightStarterBot.Extensions;
using WarLightStarterBot.ServerCommands;

namespace WarLightStarterBot.Main
{

    public class SuperRegion
    {
        public int Ranking { get; set; }
        public int ArmiesNeededToConquer { get; set; }

        public int PickRanking
        {
            get { return ((SuperRegionNames)Id).GetAttribute<PickRankAttribute>().PickRank; }
        }

        public SuperRegion(int id, int armiesReward, int ranking)
        {
            Id = id;
            ArmiesReward = armiesReward;
            SubRegions = new List<Region>();
            Neighbors = new List<SuperRegion>();
            RoundsUntilOpponentControlled = 101;
            Ranking = ranking;
        }
        public SuperRegion(int id, int armiesReward)
            : this(id, armiesReward, ((SuperRegionNames)id).GetAttribute<RankingAttribute>().Rank)
        {
        }

        public void AddSubRegion(Region subRegion)
        {
            if (!SubRegions.Contains(subRegion))
                SubRegions.Add(subRegion);
        }

        public void AddNeighbor(SuperRegion superRegion)
        {
            if (Neighbors.Contains(superRegion)) return;
            Neighbors.Add(superRegion);
            superRegion.AddNeighbor(this);
        }

        public string OwnedByPlayer()
        {
            var playerName = SubRegions.First().PlayerName;
            if (SubRegions.Any(region => playerName != region.PlayerName))
            {
                return null;
            }
            return playerName;
        }

        public int Id { get; private set; }
        public int ArmiesReward { get; private set; }
        public List<Region> SubRegions { get; private set; }
        public List<SuperRegion> Neighbors { get; private set; }
        public bool SetToBeConquered { get; set; }
        public int RoundsUntilOpponentControlled { get; set; }
        public bool KeepVisible { get; set; }

        public string Name
        {
            get
            {
                var desc = EnumExtensions.DescriptionAttr((SuperRegionNames)Id);
                return !string.IsNullOrEmpty(desc) ? desc : ((SuperRegionNames)Id).ToString();
            }
        }

        public void SetArmiesNeededToConquer()
        {
            ArmiesNeededToConquer = 0;
            foreach (var region in SubRegions)
            {
                if(region.PlayerName == Constants.UNKNOWN || region.PlayerName == Constants.NEUTRAL_PLAYER)
                    ArmiesNeededToConquer +=3;
            }
        }
    }

}