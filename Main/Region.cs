using System;
using System.Collections.Generic;
using System.Linq;
using WarLightStarterBot.Enums;
using WarLightStarterBot.Extensions;
using WarLightStarterBot.ServerCommands;

namespace WarLightStarterBot.Main
{
    public class Region
    {
        public int SubRegionRanking { get; set; }

        public bool StrategicPosition
        {
            get { return ((RegionNames)Id).GetAttribute<StrategicRegion>().IsStrategicRegion; }
        }

        public Region(int id)
        {
            Id = id;
        }
        public Region(int id, SuperRegion superRegion)
        {
            Id = id;
            SuperRegion = superRegion;
            Neighbors = new List<Region>();
            PlayerName = "unknown";
            Armies = 0;
            superRegion.AddSubRegion(this);
            SubRegionRanking = ((RegionNames)Id).GetAttribute<SubRegionsRanking>().SubRegionRank;
        }

       

        public Region(int id, SuperRegion superRegion, String playerName, int armies)
        {
            Id = id;
            SuperRegion = superRegion;
            Neighbors = new List<Region>();
            PlayerName = playerName;
            Armies = armies;
            SubRegionRanking = ((RegionNames)Id).GetAttribute<SubRegionsRanking>().SubRegionRank;
            superRegion.AddSubRegion(this);
        }

        public void AddNeighbor(Region neighbor)
        {
            if (!neighbor.SuperRegion.Equals(SuperRegion))
                SuperRegion.AddNeighbor(neighbor.SuperRegion);
            if (Neighbors.Contains(neighbor)) return;
            Neighbors.Add(neighbor);
            neighbor.AddNeighbor(this);
        }

        /**
         * @param region a Region object
         * @return True if this Region is a neighbor of given Region, false otherwise
         */
        public bool IsNeighbor(Region region)
        {
            if (Neighbors.Contains(region))
                return true;
            return false;
        }

        /**
         * @param playerName A string with a player's name
         * @return True if this region is owned by given playerName, false otherwise
         */
        public bool OwnedByPlayer(string playerName)
        {
            return playerName == PlayerName;
        }

        public List<Region> NeutralNeighbors()
        {
            return Neighbors.Where(neighbor => neighbor.OwnedByPlayer(Constants.NEUTRAL_PLAYER)).ToList();
        }

        public List<Region> NeighboringEnemies(string opponent)
        {
            return Neighbors.Where(neighbor => neighbor.OwnedByPlayer(opponent)).ToList();
        }

        public List<Region> FriendlyNeighbors()
        {
            return Neighbors.Where(neighbor => neighbor.OwnedByPlayer(PlayerName)).ToList();
        }

        public int Armies { get; set; }
        public int StartingArmies { get; set; }
        public int ArmiesLeft { get; set; }
        public int MovesToOpponent { get; set; }
        public string PlayerName { get; set; }
        public int Id { get; private set; }
        public List<Region> Neighbors { get; private set; }
        public SuperRegion SuperRegion { get; private set; }

        public bool StrategicDefenseRegion { get; set; }
        public byte StrategicDefensePriority { get; set; }
        public bool StackHere { get; set; }
        public bool HasAttacker { get; set; }

        public string Name
        {
            get
            {
                var desc = EnumExtensions.DescriptionAttr((RegionNames)Id);
                return !string.IsNullOrEmpty(desc) ? desc : ((RegionNames)Id).ToString();
            }
        }

        
    }

}