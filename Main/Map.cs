using System;
using System.Collections.Generic;
using System.Linq;
using WarLightStarterBot.ServerCommands;

namespace WarLightStarterBot.Main
{
    public class Map
    {

        private readonly List<Region> _regions;
        private readonly List<SuperRegion> _superRegions;

        public Map()
        {
            _regions = new List<Region>();
            _superRegions = new List<SuperRegion>();
        }

        public Map(List<Region> regions, List<SuperRegion> superRegions)
        {
            _regions = regions;
            _superRegions = superRegions;
        }

        /**
         * Add a Region to the map
         * @param region : Region to be Added
         */
        public void Add(Region region)
        {
            if (_regions.Contains(region))
            {
                Console.Error.WriteLine(FormattingConstants.ADD_ERROR_FORMAT, "Region");
                return;
            }
            _regions.Add(region);
        }

        /**
         * Add a SuperRegion to the map
         * @param superRegion : SuperRegion to be Added
         */
        public void Add(SuperRegion superRegion)
        {
            if (_superRegions.Any(sr => sr.Id == superRegion.Id))
            {
                Console.Error.WriteLine(FormattingConstants.ADD_ERROR_FORMAT, "SuperRegion");
                return;
            }
            _superRegions.Add(superRegion);
        }

        /**
         * @return : a new Map object exactly the same as this one
         */
        public Map GetMapCopy()
        {
            var newMap = new Map();
            
            foreach (var newSuperRegion in _superRegions.Select(sr => new SuperRegion(sr.Id, sr.ArmiesReward, sr.Ranking)))
            {
                newMap.Add(newSuperRegion);
            }
            foreach (var newRegion in _regions.Select(r => new Region(r.Id, newMap.GetSuperRegion(r.SuperRegion.Id), r.PlayerName, r.Armies)))
            {
                newMap.Add(newRegion);
            }
            foreach (var r in _regions) //Add neighbors to copied regions
            {
                var newRegion = newMap.GetRegion(r.Id);
                foreach (var neighbor in r.Neighbors)
                    newRegion.AddNeighbor(newMap.GetRegion(neighbor.Id));
            }
            return newMap;
        }

        /**
         * @param id : a Region id number
         * @return : the matching Region object
         */
        public Region GetRegion(int id)
        {
            return _regions.FirstOrDefault(region => region.Id == id);
        }

        /**
         * @param id : a SuperRegion id number
         * @return : the matching SuperRegion object
         */
        public SuperRegion GetSuperRegion(int id)
        {
            return _superRegions.FirstOrDefault(superRegion => superRegion.Id == id);
        }

        public List<Region> Regions
        {
            get { return _regions; }
        }

        public List<SuperRegion> SuperRegions
        {
            get { return _superRegions; }
        }

        public String MapString
        {
            get { return string.Join(" ", _regions.Select(region => region.Id + ";" + region.PlayerName + ";" + region.Armies)); }
        }
    }
}