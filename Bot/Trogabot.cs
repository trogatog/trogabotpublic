using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using WarLightStarterBot.Enums;
using WarLightStarterBot.Extensions;
using WarLightStarterBot.Main;
using WarLightStarterBot.Moves;
using WarLightStarterBot.ServerCommands;
// ReSharper disable AccessToForEachVariableInClosure
namespace WarLightStarterBot.Bot
{

    internal class Trogabot : IBot
    {
        private MyTurnMoves _myTurnMoves;
        private bool _opponentMovesGuessed;
        private bool _regroupArmies;
        private readonly bool _thisIsOpponent;
        private bool _winConditionMet;
        private bool _singlePlaceArmyMoveThisRound;
        private bool _staleMateMove;
        private bool _bullshitWinDetected;
        private int _possibleStaleMateRounds;
        private bool _seekAlternativeIncome;

        public Trogabot(bool asOpponent = false)
        {
            _thisIsOpponent = asOpponent;
            _singlePlaceArmyMoveThisRound = false;
            _bullshitWinDetected = false;
        }

        public List<Region> GetPreferredStartingRegions(BotState state, long timeOut)
        {
            var northAfrica = state.FullMap.GetRegion((int)RegionNames.NorthAfrica);
            //first super region pick
            var firstPicks = state.PickableStartingRegions.Where(psr => psr.SuperRegion.PickRanking == 1).ToList();
            
            //second super region pick
            var secondPicks = state.PickableStartingRegions.Where(psr => psr.SuperRegion.PickRanking == 2).ToList();

            //third super region pick
            var thirdPicks = state.PickableStartingRegions.Where(psr => psr.SuperRegion.PickRanking == 3).ToList();

            //the rest
            var fourthPicks = state.PickableStartingRegions.Where(psr => psr.SuperRegion.PickRanking == 4).ToList();
            var fifthPicks = state.PickableStartingRegions.Where(psr => psr.SuperRegion.PickRanking == 5).ToList();
            var sixthPicks = state.PickableStartingRegions.Where(psr => psr.SuperRegion.PickRanking == 6).ToList();
            //set picks
            var preferredStartingRegions = firstPicks.OrderBy(f => f.SubRegionRanking).ToList();
            preferredStartingRegions.AddRange(secondPicks.OrderBy(s => s.SubRegionRanking));

            if (firstPicks.First().IsNeighbor(firstPicks.Last()) && secondPicks.First().IsNeighbor(secondPicks.Last()) && thirdPicks.First().IsNeighbor(thirdPicks.Last()))
            {
                //I do better when there is only two opponent regions than 3...  mostly
                if (sixthPicks.Any(s => s.Id == (int)RegionNames.Siam))
                {
                    var siam = sixthPicks.First(s => s.Id == (int)RegionNames.Siam);
                    preferredStartingRegions.Add(siam);
                    sixthPicks.Remove(siam);
                }

                if (fifthPicks.Any(s => s.Id == (int)RegionNames.CentralAmerica))
                {
                    var centralAmerica = fifthPicks.First(s => s.Id == (int)RegionNames.CentralAmerica);
                    preferredStartingRegions.Add(centralAmerica);
                    fifthPicks.Remove(centralAmerica);
                }

                if (fourthPicks.Any(f => f.IsNeighbor(northAfrica)))
                {
                    foreach (var pick in fourthPicks.Where(f => f.IsNeighbor(northAfrica)).ToList())
                    {
                        preferredStartingRegions.Add(pick);
                        fourthPicks.Remove(pick);
                    }
                }
            }
            
            preferredStartingRegions.AddRange(thirdPicks.OrderBy(t => t.SubRegionRanking));

            preferredStartingRegions.AddRange(fourthPicks);

            preferredStartingRegions.AddRange(fifthPicks);

            preferredStartingRegions.AddRange(sixthPicks);

            return preferredStartingRegions.GetRange(0, 6);
        }

        private int CheckPicksForPossibleSabotage(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            if (gameState.MyRegions.Count(my => my.SuperRegion.Id == (int)SuperRegionNames.Australia) == 2)
            {
                if (gameState.PickableStartingRegions.Select(psr => psr.Id).Any(psr => psr == (int)RegionNames.Siam) && gameState.VisibleMap.GetRegion((int)RegionNames.Siam).PlayerName != gameState.MyPlayerName)
                {
                    if (!gameState.MyRegions.Any(me => gameState.VisibleMap.GetRegion((int)RegionNames.Siam).IsNeighbor(me)))
                    {
                        //at least one of my regions is going to be neighbors with Siam
                        var counterAttack = gameState.MyRegions.First(me => gameState.VisibleMap.GetRegion((int)RegionNames.Indonesia).IsNeighbor(me));
                        placeArmiesMoves[counterAttack] += armiesLeft;
                        counterAttack.Armies += armiesLeft;
                        _myTurnMoves.AttackTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, counterAttack, (int)RegionNames.Indonesia, counterAttack.Armies - 1));
                        counterAttack.Armies = 1;
                        return 0;
                    }
                }
            }
            if (gameState.VisibleMap.GetRegion((int) RegionNames.CentralAmerica).PlayerName == gameState.MyPlayerName)
            {
                var centralAmerica = gameState.VisibleMap.GetRegion((int) RegionNames.CentralAmerica);
                if (centralAmerica.Neighbors.All(n => n.PlayerName == Constants.NEUTRAL_PLAYER))
                {
                    var venezuela = gameState.VisibleMap.GetRegion((int) RegionNames.Venezuela);
                    centralAmerica.Armies += armiesLeft;
                    placeArmiesMoves[centralAmerica] += armiesLeft;
                    _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, centralAmerica, venezuela, centralAmerica.Armies - 1));
                    centralAmerica.Armies = 1;
                    return 0;
                }
            }
            if (gameState.VisibleMap.GetRegion((int) RegionNames.Siam).PlayerName == gameState.MyPlayerName && gameState.MyRegions.Count(my => my.SuperRegion.Id == (int)SuperRegionNames.Australia) == 0)
            {
                var siam = gameState.VisibleMap.GetRegion((int) RegionNames.Siam);
                if (siam.Neighbors.All(n => n.PlayerName == Constants.NEUTRAL_PLAYER))
                {
                    var indonesia = gameState.VisibleMap.GetRegion((int) RegionNames.Indonesia);
                    siam.Armies += armiesLeft;
                    placeArmiesMoves[siam] += armiesLeft;
                    _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, siam, indonesia, siam.Armies - 1));
                    siam.Armies = 1;
                    return 0;
                }
            }
            return armiesLeft;
        }
        private int PlaceStrategicDefensiveArmies(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            var myRegions = gameState.MyRegions;
            var opponent = gameState.Opponent;
            var estimatedIncome = _opponentMovesGuessed ? 0 : opponent.EstimatedIncome;
            var strategicDefenseRegions = myRegions.Where(my => my.StrategicDefensePriority == 1).ToList();
            if (!strategicDefenseRegions.Any())
            {
                strategicDefenseRegions = myRegions.Where(my => my.StrategicDefensePriority == 2).ToList();
            }

            if (strategicDefenseRegions.Count == 0) return armiesLeft;

            foreach (var region in strategicDefenseRegions.OrderByDescending(r => r.NeighboringEnemies(opponent.PlayerName).Sum(ne => ne.Armies)))
            {
                var opponentStrength = region.NeighboringEnemies(opponent.PlayerName).Sum(o => (o.Armies - 1)) + estimatedIncome;
                if (region.Armies < opponentStrength || region.Armies == 1)
                {
                    while (region.Armies < opponentStrength + 1 && armiesLeft > 0)
                    {
                        placeArmiesMoves[region]++;
                        region.Armies++;
                        armiesLeft--;
                    }

                }
            }
            return armiesLeft;
        }

        private int PlaceAggressiveArmies(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            var myRegions = gameState.MyRegions;
            var opponent = gameState.Opponent;
            var estimatedIncome = _opponentMovesGuessed ? 0 : opponent.EstimatedIncome;

            var currentSuperRegions = myRegions.Select(m => m.SuperRegion).Distinct().ToList();
            if (_thisIsOpponent && !currentSuperRegions.Any()) return armiesLeft;
            if (gameState.StartingArmies <= opponent.EstimatedIncome)
            {
                if (currentSuperRegions.OrderBy(sr => sr.Ranking).First().SubRegions.All(sr => sr.PlayerName != opponent.PlayerName && sr.PlayerName != Constants.UNKNOWN))
                    return armiesLeft;

                foreach (var super in currentSuperRegions.OrderBy(sr => sr.Ranking))
                {
                    Region saRegion = null;
                    if (gameState.MyRegions.Count(mr => mr.SuperRegion == super) == 1)
                    {
                        saRegion = gameState.MyRegions.First(my => my.SuperRegion == super);
                        if (saRegion.NeighboringEnemies(opponent.PlayerName).Count == 0)
                            continue;

                    }
                    else if (super.OwnedByPlayer() != gameState.MyPlayerName)
                    {
                        var saRegions = gameState.MyRegions.Where(my => my.SuperRegion == super).ToList();
                        saRegions = saRegions.Where(r => r.NeighboringEnemies(opponent.PlayerName).Count(n => n.SuperRegion == r.SuperRegion) == saRegions.Max(s => s.NeighboringEnemies(opponent.PlayerName).Count(n => n.SuperRegion == r.SuperRegion))).ToList();
                        saRegion = saRegions.First(r => r.Armies == saRegions.Max(sa => sa.Armies));
                    }

                    if (saRegion == null || armiesLeft == 0 || saRegion.NeighboringEnemies(opponent.PlayerName).Count == 0)
                        continue;

                    var concerningOpponent = saRegion.NeighboringEnemies(opponent.PlayerName).FirstOrDefault(ne => ne.SuperRegion.Ranking <= saRegion.SuperRegion.Ranking);
                    if (concerningOpponent == null)
                    {
                        continue;
                    }
                    var potentialOpposingArmies = saRegion.NeighboringEnemies(opponent.PlayerName).Sum(o => o.Armies) + estimatedIncome;

                    if (DefendingArmiesLeft(potentialOpposingArmies, saRegion.Armies + armiesLeft) > 0 || gameState.RoundNumber == 1)
                    {
                        var useAttacker = WhoWinsOnAttack(gameState, saRegion.Armies + armiesLeft, potentialOpposingArmies - estimatedIncome, opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Attacker;

                        if (useAttacker)
                        {
                            while (WhoWinsOnAttack(gameState, saRegion.Armies, potentialOpposingArmies - estimatedIncome, opponent.EstimatedIncome, _opponentMovesGuessed) != BattleRegions.Attacker && armiesLeft > 0)
                            {
                                saRegion.Armies++;
                                placeArmiesMoves[saRegion]++;
                                armiesLeft--;
                            }
                        }
                        else
                        {
                            while (WhoWinsOnDefend(gameState, saRegion.Armies, potentialOpposingArmies - estimatedIncome - 1, opponent.EstimatedIncome, _opponentMovesGuessed) != BattleRegions.Defender && armiesLeft > 0)
                            {
                                saRegion.Armies++;
                                placeArmiesMoves[saRegion]++;
                                armiesLeft--;
                            }
                        }
                        if (WhoWinsOnDefend(gameState, saRegion.Armies, potentialOpposingArmies - estimatedIncome - 1, opponent.EstimatedIncome, _opponentMovesGuessed) != BattleRegions.Attacker)
                        {
                            if (saRegion.NeighboringEnemies(opponent.PlayerName).Any() && saRegion.SubRegionRanking < 5)
                            {
                                foreach (var neighbor in saRegion.NeutralNeighbors().ToList())
                                    saRegion.Neighbors.Remove(neighbor);
                            }
                        }

                    }
                    if (saRegion.NeighboringEnemies(opponent.PlayerName).Count == 1)
                    {
                        var opponentRegion = saRegion.NeighboringEnemies(opponent.PlayerName).Single();
                        if (WhoWinsOnAttack(gameState, saRegion.Armies, opponentRegion.Armies, opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Attacker)
                        {
                            Console.Error.WriteLine("I expect {0}: {1} will have {2} armies while I will have {3} attacking on {4}: {5}: Attacking", opponentRegion.Id, opponentRegion.Name, opponentRegion.Armies + estimatedIncome, saRegion.Armies - 1, saRegion.Id, saRegion.Name);
                            _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, saRegion, opponentRegion, saRegion.Armies - 1));
                            saRegion.Armies = 1;
                        }
                    }
                }
            }

            return armiesLeft;
        }

        private int CompleteSuperRegions(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            if (_regroupArmies)
            {
                return armiesLeft;
            }
            var myRegions = gameState.MyRegions;
            var currentSuperRegions = myRegions.Select(m => m.SuperRegion).Distinct().OrderBy(sr => sr.Ranking).ToList();
            //check to see if any super regions can be finished this round
            foreach (var super in currentSuperRegions)
            {
                var controlledSubRegions = super.SubRegions.Where(sr => sr.PlayerName == gameState.MyPlayerName).ToList();
                var neutralControlledSubRegions = super.SubRegions.Where(sr => sr.PlayerName == Constants.NEUTRAL_PLAYER).ToList();
                //if there is one (or possibly 2) territory left and it's controled by a neutral armies, take it!!
                if (super.SubRegions.Count > controlledSubRegions.Count && super.SubRegions.Count - controlledSubRegions.Count == neutralControlledSubRegions.Count)
                {
                    if (neutralControlledSubRegions.Count == 2)
                    {
                        super.KeepVisible = true;
                        //if there are two regions left to conquer in a super, figure out how to attack them
                        //check if they share a controlled region
                        var region1 = neutralControlledSubRegions[0];
                        var region2 = neutralControlledSubRegions[1];
                        var totalNeutralArmies = neutralControlledSubRegions.Sum(n => n.Armies);

                        var availableAttackingRegions = region1.Neighbors.Where(neighbor => region2.Neighbors.Contains(neighbor) && neighbor.OwnedByPlayer(gameState.MyPlayerName)).ToList();

                        //if the super already has enough armies to finish it off, let the attack/transfer do the 2-neutral moves
                        if (super.SubRegions.Where(sr => sr.OwnedByPlayer(gameState.MyPlayerName)).Sum(sr => (sr.Armies - 1)) >= totalNeutralArmies + 3)
                        {
                            super.SetToBeConquered = true;
                            if (!availableAttackingRegions.Any(r => r.Armies >= totalNeutralArmies + 3))
                            {
                                var availableAttackers = new Dictionary<Region, List<Region>>
                                {
                                    {region1, new List<Region>()},
                                    {region2, new List<Region>()}
                                };
                                foreach (var availableAttackingRegion in super.SubRegions.Where(sr => sr.OwnedByPlayer(gameState.MyPlayerName) && sr.Armies > 2))
                                {
                                    if (availableAttackingRegion.IsNeighbor(region1) && availableAttackingRegion.Armies >= region1.Armies + 1)
                                    {
                                        availableAttackers[region1].Add(availableAttackingRegion);
                                    }
                                    if (availableAttackingRegion.IsNeighbor(region2) && availableAttackingRegion.Armies >= region2.Armies + 1)
                                    {
                                        availableAttackers[region2].Add(availableAttackingRegion);
                                    }
                                }

                                Region regionAttacked = null;
                                foreach (var attacker in availableAttackers.OrderBy(aa => aa.Value.Count))
                                {
                                    if (regionAttacked != null)
                                    {
                                        if (attacker.Value.Contains(regionAttacked))
                                        {
                                            attacker.Value.Remove(regionAttacked);
                                        }
                                    }
                                    var regionAttacking = attacker.Value.FirstOrDefault();
                                    if (regionAttacking != null)
                                    {
                                        regionAttacked = regionAttacking;
                                        _myTurnMoves.AttackTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, regionAttacking, attacker.Key, attacker.Key.Armies + 1));
                                        regionAttacking.Armies -= attacker.Key.Armies + 1;
                                        totalNeutralArmies -= attacker.Key.Armies;
                                        neutralControlledSubRegions.Remove(attacker.Key);
                                        attacker.Key.MakeMeInvisible();
                                    }
                                    else
                                    {
                                        super.SetToBeConquered = false;
                                    }
                                }
                            }
                            else
                            {
                                super.SetToBeConquered = false;
                            }
                        }
                        if (availableAttackingRegions.Any() && !super.SetToBeConquered)
                        {
                            var attackingRegion = availableAttackingRegions.First(r => r.Armies == availableAttackingRegions.Max(a => a.Armies));

                            while (armiesLeft > 0 && attackingRegion.Armies < totalNeutralArmies + neutralControlledSubRegions.Count + 1)
                            {
                                armiesLeft--;
                                attackingRegion.Armies++;
                                placeArmiesMoves[attackingRegion]++;
                            }

                            if (attackingRegion.Armies > totalNeutralArmies + 3)
                            {
                                var armiesToAttack = (attackingRegion.Armies - 1) / 2;
                                foreach (var regionToConquer in neutralControlledSubRegions.OrderByDescending(n => n.SubRegionRanking))
                                {
                                    _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(gameState.MyPlayerName, attackingRegion, regionToConquer, armiesToAttack));
                                    attackingRegion.Armies -= armiesToAttack;
                                    regionToConquer.MakeMeInvisible();
                                }
                                super.SetToBeConquered = true;
                            }
                            else
                            {
                                foreach (var regionToConquer in neutralControlledSubRegions.OrderBy(n => n.SubRegionRanking))
                                {
                                    if (attackingRegion.Armies >= regionToConquer.Armies + 2)
                                    {
                                        _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(gameState.MyPlayerName, attackingRegion, regionToConquer, regionToConquer.Armies + 1));
                                        attackingRegion.Armies -= regionToConquer.Armies + 1;
                                        if (placeArmiesMoves.Any(p => p.Value > 0))
                                        {
                                            neutralControlledSubRegions.Remove(regionToConquer);
                                            regionToConquer.MakeMeInvisible();
                                        }
                                        super.SetToBeConquered = true;
                                    }
                                    else
                                    {
                                        super.SetToBeConquered = false;
                                        Console.Error.WriteLine("Round {0}: Not enough armies left to finish {1}. Holding armies at {2}: {3}", gameState.RoundNumber, super.Name, attackingRegion.Id, attackingRegion.Name);
                                    }
                                }
                                if (gameState.RoundNumber == 1)
                                {
                                    //if I'm here, I have control of both regions of Australia or South Africa. Pull any armies in neighboring regions here to better place armies next round
                                    foreach (var friendlyNeighbor in attackingRegion.FriendlyNeighbors())
                                    {
                                        _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, friendlyNeighbor, attackingRegion, friendlyNeighbor.Armies - 1));
                                        friendlyNeighbor.Armies = 1;
                                    }
                                }
                            }
                        }
                    }
                    if (neutralControlledSubRegions.Count == 1)
                    {
                        var unownedRegion = super.SubRegions.First(sr => sr.PlayerName != gameState.MyPlayerName);
                        var mySurroundingRegions = unownedRegion.Neighbors.Where(n => n.OwnedByPlayer(gameState.MyPlayerName) && !n.StrategicDefenseRegion).ToList();
                        var takeWithRegion = mySurroundingRegions.First(r => r.Armies == mySurroundingRegions.Max(msr => msr.Armies));

                        //take a 1 with 2, take a 2 with 3
                        while (armiesLeft > 0 && takeWithRegion.Armies < unownedRegion.Armies + 2)
                        {
                            placeArmiesMoves[takeWithRegion]++;
                            armiesLeft--;
                            takeWithRegion.Armies++;
                        }
                        if (takeWithRegion.Armies >= unownedRegion.Armies + 2)
                        {
                            _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(gameState.MyPlayerName, takeWithRegion, unownedRegion, unownedRegion.Armies + 1));
                            takeWithRegion.Armies -= unownedRegion.Armies + 1;

                            if (placeArmiesMoves.Any(p => p.Value > 0))
                                unownedRegion.MakeMeInvisible();

                            super.SetToBeConquered = true;
                        }
                    }
                }
            }
            return armiesLeft;
        }

        private int HoldUpASingleEnemy(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            var opponent = gameState.Opponent;
            var opposingRegions = opponent.Regions.Where(r => r.NeighboringEnemies(gameState.MyPlayerName).Any()).ToList();
            var myRegionsWithOpponents = gameState.MyRegions.Where(my => my.NeighboringEnemies(opponent.PlayerName).Count > 0).ToList();
            if (!myRegionsWithOpponents.Any())
                return armiesLeft;
            if (opposingRegions.Count == 1 || myRegionsWithOpponents.Count == 1)
            {
                var opposingRegion = opposingRegions.First(o => o.NeighboringEnemies(gameState.MyPlayerName).Any());
                var myOpposingRegions = opposingRegion.Neighbors.Where(n => n.OwnedByPlayer(gameState.MyPlayerName)).ToList();

                var myOpposingRegion = myOpposingRegions.FirstOrDefault(mor => mor.Armies == myOpposingRegions.Max(m => m.Armies));
                if (myOpposingRegion != null)
                {
                    //if (WhoWinsOnDefend(gameState, myOpposingRegion.Armies + armiesLeft, opposingRegions.Sum(o => (o.Armies-1)) - opposingRegions.Count, opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Attacker)
                    //{
                    //    //no matter what we do, this won't end up good
                    //    if (gameState.MyRegions.Count > 1)
                    //    {
                    //        gameState.MyRegions.Remove(myOpposingRegion);
                    //        myOpposingRegion.MakeMeInvisible(true);
                    //    }
                    //    return armiesLeft;
                    //}
                    placeArmiesMoves[myOpposingRegion] += armiesLeft;
                    myOpposingRegion.Armies += armiesLeft;
                    armiesLeft = 0;

                    if (WhoWinsOnDefend(gameState, myOpposingRegion.Armies, opposingRegions.Sum(o => o.Armies) - opposingRegions.Count, opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender)
                    {
                        foreach (var region in myOpposingRegion.FriendlyNeighbors().Where(fn => fn.Armies > 1).OrderByDescending(fn => fn.Armies))
                        {
                            if (region.NeighboringEnemies(opponent.PlayerName).Count == 0)
                            {
                                _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, region, myOpposingRegion, region.Armies - 1));
                                region.Armies = 1;
                            }
                        }
                    }
                }

            }
            return armiesLeft;
        }

        private int AttackNeighboringSuper(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            var myRegions = gameState.MyRegions;
            var currentSuperRegions = myRegions.Select(m => m.SuperRegion).Distinct().ToList();
            var superNeighbors = new List<SuperRegion>();
            foreach (var super in currentSuperRegions)
            {
                foreach (var neighbor in super.Neighbors)
                    if (!superNeighbors.Contains(neighbor) && !neighbor.SetToBeConquered && neighbor.OwnedByPlayer() != gameState.MyPlayerName)
                        superNeighbors.Add(neighbor);
            }

            SuperRegion nextAttack;

            if (_seekAlternativeIncome && superNeighbors.Count > 1)
            {
                superNeighbors = superNeighbors.OrderBy(s => s.Ranking).ToList();
                nextAttack = superNeighbors[1];
                if(nextAttack != null && nextAttack.SubRegions.All(sr => sr.PlayerName == Constants.NEUTRAL_PLAYER || sr.PlayerName == Constants.UNKNOWN))
                {
                    var possibleAttackingRegions = myRegions.Where(my => my.Neighbors.Any(n => n.SuperRegion.Id == nextAttack.Id)).ToList();
                    var attackingRegion = possibleAttackingRegions.FirstOrDefault(my => my.Armies == possibleAttackingRegions.Max(p => p.Armies));
                    if (attackingRegion != null)
                    {
                        placeArmiesMoves[attackingRegion] += armiesLeft;
                        attackingRegion.Armies += armiesLeft;
                        armiesLeft = 0;
                        var nextAttackMove = attackingRegion.NeutralNeighbors().First(n => n.SuperRegion.Id == nextAttack.Id);
                        _myTurnMoves.AttackTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, attackingRegion, nextAttackMove, 4));
                        attackingRegion.Armies -= 4;
                    }
                }
            }
            else
            {
                nextAttack = superNeighbors.SingleOrDefault(sn => sn.Ranking == superNeighbors.Min(s => s.Ranking));


                if (nextAttack != null && gameState.MyRegions.All(my => my.SuperRegion != nextAttack))
                {
                    nextAttack.KeepVisible = true;
                    var possibleAttackingRegions = myRegions.Where(my => my.Neighbors.Any(n => n.SuperRegion.Id == nextAttack.Id)).ToList();
                    var attackingRegion = possibleAttackingRegions.FirstOrDefault(my => my.Armies == possibleAttackingRegions.Max(p => p.Armies));

                    if (attackingRegion != null)
                    {
                        placeArmiesMoves[attackingRegion] += armiesLeft;
                        attackingRegion.Armies += armiesLeft;
                        armiesLeft = 0;
                        attackingRegion.StackHere = true;
                        var northAfrica = gameState.VisibleMap.GetRegion((int)RegionNames.NorthAfrica);
                        if (nextAttack.Id == (int)SuperRegionNames.Africa && northAfrica.OwnedByPlayer(Constants.NEUTRAL_PLAYER))
                        { 
                            //this should only happen early game when a region is in Europe and I'm facing 2 neighbors
                            if(attackingRegion.IsNeighbor(northAfrica) && attackingRegion.Armies - 1 > northAfrica.Armies)
                            {
                                Console.Error.WriteLine("Round {0}: Hit early routine to take North Africa with European region", gameState.RoundNumber);
                                _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(gameState.MyPlayerName, attackingRegion, northAfrica, attackingRegion.Armies - 1));
                                attackingRegion.Armies = 1;
                            }
                        }                        
                    }
                }
            }
            return armiesLeft;
        }

        private int SeekOutOpponent(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            if (gameState.Opponent.KnownOwnedRegions.Regions.Any())
            {
                foreach (var region in gameState.VisibleMap.Regions.Where(ur => ur.NeighboringEnemies(gameState.Opponent.PlayerName).Count > 0).Union(gameState.Opponent.KnownOwnedRegions.Regions))
                {
                    region.MovesToOpponent = 0;
                    IncrementNeighbors(gameState, region.Id, 1);
                }

                var closestRegions = gameState.MyRegions.Where(my => my.MovesToOpponent == gameState.MyRegions.Min(r => r.MovesToOpponent)).ToList();
                var closestRegion = closestRegions.First(cr => cr.Armies == closestRegions.Max(c => c.Armies));
                var regionToPress = closestRegion.Neighbors.FirstOrDefault(n => n.MovesToOpponent == closestRegion.MovesToOpponent - 1);
                if (regionToPress != null)
                {
                    placeArmiesMoves[closestRegion] += armiesLeft;
                    closestRegion.Armies += armiesLeft;
                    armiesLeft = 0;
                    if (closestRegion.Armies > regionToPress.Armies + 1)
                    {
                        _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, closestRegion, regionToPress, closestRegion.Armies - 1));
                        closestRegion.Armies = 1;
                    }
                }
            }
            return armiesLeft;
        }

        private int CheckForStaleMate(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            //If I don't control North Africa, this is highly likely
            var northAfrica = gameState.VisibleMap.GetRegion((int) RegionNames.NorthAfrica);
            if (northAfrica.OwnedByPlayer(gameState.MyPlayerName) && northAfrica.Armies == gameState.MyRegions.Max(my => my.Armies))
            {
                //nope, this won't be a stalemate
                return armiesLeft;
            }
            if (_possibleStaleMateRounds > 0)
            {
                //Where is my big stack?
                var myBigStack = northAfrica.NeighboringEnemies(gameState.MyPlayerName).FirstOrDefault(my => my.Armies == northAfrica.NeighboringEnemies(gameState.MyPlayerName).Max(r => r.Armies));
                if (myBigStack == null || !myBigStack.NeighboringEnemies(gameState.Opponent.PlayerName).Any())
                {
                    //return armiesLeft;
                    //stalemate is somewhere else
                    //see if it's just one region that I'm kissing up against
                    var regionsWithOnlyOneOpponent = gameState.MyRegions.Where(my => my.NeighboringEnemies(gameState.Opponent.PlayerName).Count == 1).ToList();
                    if (regionsWithOnlyOneOpponent.Count() == 1)
                    {
                        //only one region with only one opponent (see game http://theaigames.com/competitions/warlight-ai-challenge/games/5377033b4b5ab24d426141a9)
                        var myRegion = regionsWithOnlyOneOpponent.First();
                        var myNeighbor = myRegion.NeighboringEnemies(gameState.Opponent.PlayerName).First();
                        if (WhoWinsOnAttack(gameState, myRegion.Armies + gameState.StartingArmies, myNeighbor.Armies, gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender
                            &&
                            WhoWinsOnDefend(gameState, myRegion.Armies + gameState.StartingArmies, myNeighbor.Armies, gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender)
                        {
                            //neither attacking region can win a battle right now
                            var j = 0;
                            while (WhoWinsOnDefend(gameState, myRegion.Armies + j, myNeighbor.Armies, gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) != BattleRegions.Defender)
                            {
                                j++;
                            }
                            if (j == 0)
                            {
                                var currentSupers = gameState.MyRegions.Select(my => my.SuperRegion).Distinct().OrderBy(sr => sr.Ranking);
                                Region regionToUse = null;
                                foreach (var super in currentSupers)
                                {
                                    if(super.SubRegions.Where(sr => sr.PlayerName == gameState.MyPlayerName).All(my => my.NeighboringEnemies(gameState.Opponent.PlayerName).Count > 0))
                                        continue;

                                    regionToUse =
                                        super.SubRegions.OrderByDescending(sr => sr.Armies)
                                            .First(sr => sr.PlayerName == gameState.MyPlayerName);
                                    break;
                                }
                                if (regionToUse != null)
                                {
                                    placeArmiesMoves[regionToUse] += 3;
                                    regionToUse.Armies += 3;
                                    armiesLeft -= 3;
                                }
                            }
                        }
                    }

                    return armiesLeft;
                }
                //North Africa is where stalemates happen in this round
                if (myBigStack.SuperRegion.Id == (int) SuperRegionNames.Africa || (myBigStack.SuperRegion.Id == (int)SuperRegionNames.Europe && gameState.VisibleMap.GetSuperRegion((int)SuperRegionNames.Europe).OwnedByPlayer() == gameState.MyPlayerName))
                {
                    var myAsiaRegions = gameState.MyRegions.Where(my => my.SuperRegion.Id == (int)SuperRegionNames.Asia).ToList();
                    var myNorthAmericaRegions = gameState.MyRegions.Where(my => my.SuperRegion.Id == (int)SuperRegionNames.NorthAmerica).ToList();
                    if (northAfrica.OwnedByPlayer(gameState.Opponent.PlayerName) &&
                        gameState.MyRegions.Any(r => r.IsNeighbor(northAfrica)) &&
                        gameState.MyRegions.Any(
                            my => my.Neighbors.Any(n => n.SuperRegion.Id == (int) SuperRegionNames.Asia)))
                    {
                        //are my biggest stack and his biggest stack roughly neck and neck
                        if (
                            WhoWinsOnAttack(gameState, myBigStack.Armies + gameState.StartingArmies, northAfrica.Armies,
                                gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender
                            &&
                            WhoWinsOnDefend(gameState, myBigStack.Armies + gameState.StartingArmies, northAfrica.Armies,
                                gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender)
                        {
                            //neither attacking region can win a battle right now, I need to start pushing Asia HARD towards North America
                            var nextTarget = (int) RegionNames.China;
                            if (gameState.Opponent.KnownOwnedRegions.Regions.All(r => r.Id != (int)RegionNames.Kamchatka))
                            {
                                if (gameState.MyRegions.All(my => my.Id != (int)RegionNames.Mongolia))
                                {
                                    if (gameState.Opponent.KnownOwnedRegions.Regions.All(r => r.Id != (int) RegionNames.Kamchatka))
                                    {
                                        gameState.Opponent.KnownOwnedRegions.Add(gameState.VisibleMap.GetRegion((int)RegionNames.Kamchatka));
                                        nextTarget = (int) RegionNames.Mongolia;
                                    }
                                }
                                if (gameState.MyRegions.All(my => my.Id != (int)RegionNames.Kamchatka))
                                {
                                    if (gameState.Opponent.KnownOwnedRegions.Regions.All(r => r.Id != (int) RegionNames.Alaska))
                                    {
                                        gameState.Opponent.KnownOwnedRegions.Add(gameState.VisibleMap.GetRegion((int)RegionNames.Alaska));
                                        nextTarget = (int) RegionNames.Alaska;
                                    }
                                }
                                if (gameState.MyRegions.All(my => my.Id != (int)RegionNames.Alaska))
                                {
                                    if (gameState.Opponent.KnownOwnedRegions.Regions.All(r => r.Id != (int) RegionNames.Alberta))
                                    {
                                        gameState.Opponent.KnownOwnedRegions.Add(gameState.VisibleMap.GetRegion((int)RegionNames.Alberta));
                                    }
                                }
                            }

                            if (gameState.MyRegions.Any(my => my.Id == (int)RegionNames.Kamchatka))
                                gameState.VisibleMap.GetSuperRegion((int)SuperRegionNames.NorthAmerica).KeepVisible = true;

                            
                            gameState.VisibleMap.GetRegion(nextTarget).MovesToOpponent = 0;
                            IncrementNeighbors(gameState, nextTarget, 1);
                            
                            foreach (var region in gameState.FullMap.GetSuperRegion((int)SuperRegionNames.Asia).SubRegions)
                            {
                                //stops me from having a transfer to a lower subregion happen. I just want this routine to 
                                //focus on interrupting the enemy
                                region.SubRegionRanking = 8;
                                gameState.VisibleMap.GetRegion(region.Id).SubRegionRanking = 8;
                            }
                            var j = 0;
                            // because I might have enough armies sitting on Indonesia 
                            if (gameState.MyRegions.All(my => my.Id != (int)RegionNames.Siam))
                            {
                                gameState.VisibleMap.GetSuperRegion((int)SuperRegionNames.Asia).KeepVisible = true;
                            }
                            while (
                                WhoWinsOnDefend(gameState, myBigStack.Armies + j, northAfrica.Armies,
                                    gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) != BattleRegions.Defender)
                            {
                                j++;
                            }
                            if (j == 0 && gameState.VisibleMap.GetSuperRegion((int) SuperRegionNames.Asia).OwnedByPlayer() != gameState.MyPlayerName)
                            {
                                Region nextRegionToUse;
                                
                                if (!myAsiaRegions.Any())
                                {
                                    nextRegionToUse =
                                        gameState.MyRegions.FirstOrDefault(my => my.Id == (int) RegionNames.Indonesia) ??
                                        myBigStack;
                                }
                                else
                                {
                                    var nextAttack = gameState.VisibleMap.GetRegion(nextTarget);
                                    nextRegionToUse = myNorthAmericaRegions.Any() ? myNorthAmericaRegions.FirstOrDefault(my => my.NeighboringEnemies(gameState.Opponent.PlayerName).Count == myNorthAmericaRegions.Max(n => n.NeighboringEnemies(gameState.Opponent.PlayerName).Count)) : myAsiaRegions.First(my => my.IsNeighbor(nextAttack) && my.Armies == nextAttack.NeighboringEnemies(gameState.MyPlayerName).Max(m => m.Armies));
                                    if (nextRegionToUse == null) return armiesLeft;
                                }

                                Region nextRegionToTake;
                                if (nextRegionToUse.Id == (int)RegionNames.Kamchatka) //hit our target. 
                                {
                                    //and I'm not already in North America
                                    if (gameState.MyRegions.Any(my => my.SuperRegion.Id == (int)SuperRegionNames.NorthAmerica))
                                    {
                                        _staleMateMove = false;
                                        return armiesLeft;
                                    }
                                    
                                    nextRegionToTake = gameState.VisibleMap.GetRegion((int)RegionNames.Alaska);
                                    placeArmiesMoves[nextRegionToUse] += armiesLeft;
                                    nextRegionToUse.Armies += armiesLeft;
                                    _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, nextRegionToUse, nextRegionToTake, nextRegionToUse.Armies - 1));
                                    nextRegionToUse.Armies = 1;
                                    return 0;
                                }
                                if (myNorthAmericaRegions.Any())
                                {
                                    nextRegionToUse.Armies += 4;
                                    placeArmiesMoves[nextRegionToUse] += 4;
                                    armiesLeft -= 4;
                                    return armiesLeft;
                                }
                                nextRegionToTake = gameState.VisibleMap.GetRegion(nextTarget);
                                    //nextRegionToUse.NeutralNeighbors()
                                    //    .FirstOrDefault(n => n.SuperRegion.Id == (int)SuperRegionNames.Asia && n.MovesToOpponent == nextRegionToUse.NeutralNeighbors().Min(ne => ne.MovesToOpponent));

                                if (nextRegionToTake == null)
                                {
                                    //Likely hit when I make it to Kamchatka
                                    return armiesLeft;
                                }
                                _staleMateMove = true;
                                placeArmiesMoves[nextRegionToUse] += 3;
                                nextRegionToUse.Armies += 3;
                                armiesLeft -= 3;
                                if (nextRegionToTake.PlayerName == gameState.Opponent.PlayerName && WhoWinsOnAttack(gameState, nextRegionToUse.Armies - 1, nextRegionToTake.Armies, gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) != BattleRegions.Attacker)
                                {
                                    return armiesLeft;
                                }
                                
                                _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(gameState.MyPlayerName, nextRegionToUse, nextRegionToTake, nextRegionToUse.Id == myBigStack.Id ? 3 : nextRegionToUse.Armies - 1));
                                nextRegionToUse.Armies = 1;
                                
                                return armiesLeft;
                            }
                        }
                        myBigStack.Armies += armiesLeft;
                        placeArmiesMoves[myBigStack] += armiesLeft;
                        return 0;
                    }
                }
                else if (myBigStack.SuperRegion.Id == (int) SuperRegionNames.Europe)
                {
                    if (northAfrica.OwnedByPlayer(gameState.Opponent.PlayerName))
                    {
                        //are my biggest stack and his biggest stack roughly neck and neck
                        if (
                            WhoWinsOnAttack(gameState, myBigStack.Armies + gameState.StartingArmies, northAfrica.Armies,
                                gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender
                            &&
                            WhoWinsOnDefend(gameState, myBigStack.Armies + gameState.StartingArmies, northAfrica.Armies,
                                gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender)
                        {
                            //neither attacking region can win a battle right now, I need to start pushing Asia HARD
                            var j = 0;
                            while (WhoWinsOnDefend(gameState, myBigStack.Armies + j, northAfrica.Armies, gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) != BattleRegions.Defender)
                            {
                                j++;
                            }
                            if (j == 0 && gameState.VisibleMap.GetSuperRegion((int) SuperRegionNames.Europe).OwnedByPlayer() != gameState.MyPlayerName)
                            {
                                var nextRegionToUse = gameState.MyRegions.FirstOrDefault(my => my.NeutralNeighbors().Any(n => n.SuperRegion.Id == (int) SuperRegionNames.Europe) && my.Id != myBigStack.Id) ?? myBigStack;

                                var nextRegionToTake = nextRegionToUse.NeutralNeighbors().FirstOrDefault(n => n.SuperRegion.Id == (int) SuperRegionNames.Europe);
                                if (nextRegionToTake == null)
                                {
                                    Console.Error.WriteLine("Round {0}: Something weird happened and I didn't find a European neighbor", gameState.RoundNumber);
                                }
                                _staleMateMove = true;
                                placeArmiesMoves[nextRegionToUse] += 3;
                                nextRegionToUse.Armies += 3;
                                armiesLeft -= 3;
                                _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, nextRegionToUse, nextRegionToTake, nextRegionToUse.Id == myBigStack.Id ? 3 : nextRegionToUse.Armies - 1));
                                nextRegionToUse.Armies = nextRegionToUse.Id == myBigStack.Id ? nextRegionToUse.Armies - 3 : 1;
                                return armiesLeft;
                            }
                            if (j < armiesLeft)
                            {
                                placeArmiesMoves[myBigStack] += armiesLeft;
                                myBigStack.Armies += armiesLeft;
                                return 0;
                            }
                        }
                        if (gameState.VisibleMap.GetSuperRegion((int)SuperRegionNames.Europe).OwnedByPlayer() == gameState.MyPlayerName && gameState.MyRegions.All(my => my.SuperRegion.Id != (int)SuperRegionNames.NorthAmerica))
                        {
                            if (gameState.StartingArmies > gameState.Opponent.EstimatedIncome)
                            {
                                var iceland = gameState.MyRegions.First(my => my.Id == (int)RegionNames.Iceland);
                                var attack = iceland.Neighbors.First(n => n.SuperRegion.Id == (int)SuperRegionNames.NorthAmerica);
                                placeArmiesMoves[iceland] += armiesLeft;
                                iceland.Armies += armiesLeft;
                                iceland.Armies = 1;
                                var southernEurope = gameState.MyRegions.First(my => my.Id == (int)RegionNames.SouthernEurope);
                                foreach (var friendly in southernEurope.FriendlyNeighbors().Where(f => f.Armies > 1))
                                {
                                    _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, friendly, southernEurope, friendly.Armies - 1));
                                    friendly.Armies = 1;
                                }
                                //attack to greenland should be last just in case they do not own the super already
                                _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, iceland, attack, iceland.Armies - 1));
                                return 0;
                            }
                        }
                    }
                }
                else if(myBigStack.Neighbors.Any(n => n.SuperRegion.Id != myBigStack.SuperRegion.Id))
                {
                    var neighboringSuperSubRegion = myBigStack.Neighbors.OrderBy(s => s.SuperRegion.ArmiesReward).First(n => n.SuperRegion.Id != myBigStack.SuperRegion.Id);
                    if (neighboringSuperSubRegion.OwnedByPlayer(Constants.NEUTRAL_PLAYER))
                    {
                        neighboringSuperSubRegion.SuperRegion.SetArmiesNeededToConquer();
                        var armyCompare = myBigStack.NeighboringEnemies(gameState.Opponent.PlayerName).FirstOrDefault();
                        if (armyCompare != null)
                        {
                            var j = 0;
                            while (WhoWinsOnDefend(gameState, myBigStack.Armies - j, armyCompare.Armies - 1, gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender)
                            {
                                j++;
                            }
                            if (j > neighboringSuperSubRegion.SuperRegion.ArmiesNeededToConquer)
                            {
                                placeArmiesMoves[myBigStack] += armiesLeft;
                                myBigStack.Armies += armiesLeft;
                                _myTurnMoves.AttackTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, myBigStack, neighboringSuperSubRegion, neighboringSuperSubRegion.SuperRegion.ArmiesNeededToConquer));
                                myBigStack.Armies -= neighboringSuperSubRegion.SuperRegion.ArmiesNeededToConquer;
                                neighboringSuperSubRegion.SuperRegion.KeepVisible = true;
                                return 0;
                            }
                        }
                    }
                }
            }
            return armiesLeft;
        }

        private int _regionsControlled;
        private int CheckForBullshitTakeover(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            if (gameState.RoundNumber == 1)
            {
                _regionsControlled = gameState.MyRegions.Count;
                return armiesLeft;
            }
            //basically, if an opponent has a bullshit win of a region on an early turn, like, takes a 9 with an 11,
            //fuck it, I've lost unless I do something!
            if (gameState.MyRegions.Count < _regionsControlled)
            {
                foreach (var move in gameState.Opponent.AttackTransferMovesForRound[gameState.RoundNumber - 1])
                {

                }
            }
            return armiesLeft;
        }
        private int CheckForWinCondition(BotState gameState, IDictionary<Region, int> placeArmiesMoves, int armiesLeft)
        {
            var me = gameState.MyPlayerName;
            var northAfrica = gameState.VisibleMap.Regions.First(r => r.Id == (int)RegionNames.NorthAfrica);
            if (gameState.MyRegions.All(r => r.Id != northAfrica.Id))
            {
                if (gameState.StartingArmies > gameState.Opponent.EstimatedIncome)
                {
                    var naNeighbors = gameState.MyRegions.Where(my => my.Neighbors.Any(n => n.Id == northAfrica.Id)).ToList();
                    if (naNeighbors.Any())
                    {
                        var bestShot = naNeighbors.First(my => my.Armies == naNeighbors.Max(na => na.Armies));

                        if (WhoWinsOnAttack(gameState, bestShot.Armies + armiesLeft, gameState.VisibleMap.GetRegion((int) RegionNames.NorthAfrica).Armies, gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Attacker || northAfrica.PlayerName == Constants.NEUTRAL_PLAYER)
                        {
                            if (bestShot.NeighboringEnemies(gameState.Opponent.PlayerName).All(n => n.Id == northAfrica.Id))
                            {
                                placeArmiesMoves[bestShot] += armiesLeft;
                                bestShot.Armies += armiesLeft;
                                _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(gameState.MyPlayerName, bestShot, northAfrica, bestShot.Armies - 1));
                                bestShot.Armies = 1;
                                return 0;
                            }
                        }
                    }
                }
                return armiesLeft;
            }

            //Win condition #1 -- I own Australia and North Africa. Keep placing armies there until I control All of Africa or ca break south America
            if (gameState.VisibleMap.GetSuperRegion((int)SuperRegionNames.Australia).OwnedByPlayer() == me)
            {
                //Opponent also needs to be in South America
                if (gameState.Opponent.KnownOwnedRegions.Regions.Any(r => r.SuperRegion.Id == (int)SuperRegionNames.SouthAmerica))
                {
                    //TODO:this needs a better home.  Useful everywhere, but needs to not be added to potential attackers, just defenders
                    //check if opponent snuck away to fool me
                    foreach (var transferMove in gameState.Opponent.AttackTransferMovesForRound[gameState.RoundNumber - 1])
                    {
                        if (gameState.VisibleMap.GetRegion(transferMove.FromRegion.Id).OwnedByPlayer(gameState.Opponent.PlayerName))
                        {
                            if (transferMove.FromRegion.IsNeighbor(gameState.VisibleMap.GetRegion((int)RegionNames.NorthAfrica)))
                            {
                                if (gameState.VisibleMap.GetRegion(transferMove.ToRegion.Id).OwnedByPlayer(Constants.UNKNOWN) || gameState.VisibleMap.GetRegion(transferMove.ToRegion.Id).OwnedByPlayer(gameState.Opponent.PlayerName))
                                {
                                    //expect those armies to come back
                                    transferMove.FromRegion.Armies += transferMove.Armies;
                                }
                            }
                        }
                    }
                    if (_regroupArmies)
                    {
                        var africa = gameState.VisibleMap.GetSuperRegion((int) SuperRegionNames.Africa);
                        if (africa.OwnedByPlayer() != me)
                        {
                            foreach (var friendly in northAfrica.FriendlyNeighbors().Where(fn => fn.Armies > 1))
                            {
                                _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName,
                                    friendly, northAfrica, friendly.Armies - 1));
                                friendly.Armies = 1;
                            }
                        }
                    }
                    var i = 0;
                    while (WhoWinsOnDefend(gameState, northAfrica.Armies + i, northAfrica.NeighboringEnemies(gameState.Opponent.PlayerName).Sum(o => (o.Armies - 1)), gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) != BattleRegions.Defender)
                    {
                        i++;
                    }
                    if (i == 0)
                    {
                        foreach (var toRegion in northAfrica.NeighboringEnemies(gameState.Opponent.PlayerName))
                        {
                            var regionsRemaining = new Dictionary<Region, int>();
                            var maxArmiesToAttack = northAfrica.ArmiesLeft > toRegion.Armies * 2 ? toRegion.Armies * 2 : northAfrica.ArmiesLeft;
                            double naArmiesLeft = northAfrica.Armies - maxArmiesToAttack;
                            foreach (var neighbor in northAfrica.NeighboringEnemies(gameState.Opponent.PlayerName).Where(n => n != toRegion).OrderByDescending(n => n.Armies))
                            {
                                naArmiesLeft = AttackingArmiesLeft((int)naArmiesLeft, neighbor.Armies - 1);
                                regionsRemaining.Add(neighbor, (int)naArmiesLeft);
                            }

                            if (regionsRemaining.Count > 1)
                            {
                                var first = regionsRemaining.First().Value;
                                if (regionsRemaining.All(r => r.Value == first))
                                {
                                    //start feeding into the next possible super region
                                    _seekAlternativeIncome = true;
                                    break;
                                }
                            }
                        }
                       

                        _winConditionMet = true;
                        return armiesLeft;
                    }
                    if (i <= armiesLeft)
                    {
                        _winConditionMet = true;
                        placeArmiesMoves[northAfrica] += armiesLeft;
                        northAfrica.Armies += armiesLeft;
                        if (_regroupArmies)
                        { 
                            northAfrica.Armies = 1;
                            _regroupArmies = false;
                        }
                        return 0;
                    }
                    var friendlyArmies = northAfrica.FriendlyNeighbors().Sum(f => (f.Armies - 1));
                    if (i <= armiesLeft + friendlyArmies)
                    {
                        _winConditionMet = true;
                        placeArmiesMoves[northAfrica] += armiesLeft;
                        northAfrica.Armies += armiesLeft;
                        //transfer all local armies to North Africa. Win condition is met, so I'm not worried about losing the territories
                        foreach (var friendly in northAfrica.FriendlyNeighbors().Where(fn => fn.Armies > 1))
                        {
                            _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(gameState.MyPlayerName, friendly, northAfrica, friendly.Armies - 1));
                            friendly.Armies = 1;
                        }
                        return 0;
                    }
                }
            }
            //Win condition #2 -- First I have to own South America
            if (gameState.VisibleMap.GetSuperRegion((int)SuperRegionNames.SouthAmerica).OwnedByPlayer() == me)
            {
                //and opponent is in Australia
                if (gameState.Opponent.KnownOwnedRegions.Regions.Any(r => r.SuperRegion.Id == (int)SuperRegionNames.Australia))
                {
                    //Win condition #2.1 -- No opponent in sight
                    if (gameState.MyRegions.All(my => my.NeighboringEnemies(gameState.Opponent.PlayerName).Count == 0))
                    {
                        //Do I know where opponent is?
                        if (gameState.Opponent.KnownOwnedRegions.Regions.Any())
                        {
                            foreach (var region in gameState.Opponent.KnownOwnedRegions.Regions)
                            {
                                region.MovesToOpponent = 0;
                                IncrementNeighbors(gameState, region.Id, 1);
                            }
                            //yup, I do. Pile up armies and go, unless it's in north america, then move it home
                            var bigFattyArmy = gameState.MyRegions.OrderByDescending(my => my.Armies).First();
                            var goKickAss = bigFattyArmy.Neighbors.OrderBy(my => my.MovesToOpponent).First();
                            if (goKickAss.SuperRegion.Id == (int)SuperRegionNames.NorthAmerica)
                            {
                                placeArmiesMoves[northAfrica] += armiesLeft;
                                //northAfrica.Armies = 1;
                                return 0;
                            }
                            
                            placeArmiesMoves[bigFattyArmy] += armiesLeft;
                            bigFattyArmy.Armies += armiesLeft;
                            _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, bigFattyArmy, goKickAss, bigFattyArmy.Armies - 1));
                            bigFattyArmy.Armies = 1;
                            foreach (var neighbor in goKickAss.Neighbors.Where(n => n.PlayerName == me && n.Armies > 1))
                            {
                                _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, neighbor, goKickAss, neighbor.Armies - 1));
                                neighbor.Armies = 1;
                            }
                            goKickAss.PlayerName = gameState.Opponent.PlayerName; //to move other armies closer?
                            return 0;
                            
                        }
                    }
                    //Win condition #2.2 -- Opponent up against me wanting to get through
                    else
                    {
                        //if I have an opponent in North America, don't continue. Clear him out first
                        if (gameState.MyRegions.Any(my => my.NeighboringEnemies(gameState.Opponent.PlayerName).Any(op => op.SuperRegion.Id == (int)SuperRegionNames.NorthAmerica)))
                            return armiesLeft;

                        //if my income is greater than my opponents, I've won.
                        if (gameState.StartingArmies > gameState.Opponent.EstimatedIncome)
                            return armiesLeft;

                        gameState.VisibleMap.GetSuperRegion((int) SuperRegionNames.NorthAmerica).KeepVisible = true;
                        
                        //NOPE Bad idea
                        //if I made it to middle east or india, don't worry about this. I am probably big enough to push them back into Australia
                        //if (gameState.MyRegions.Any(my => my.SuperRegion.Id == (int) SuperRegionNames.Asia))
                        //    return armiesLeft;
                        //NOPE

                        var j = 0;
                        
                        var visibleArmies = gameState.Opponent.Regions.Where(r => r.NeighboringEnemies(me).Any()).Sum(o => (o.Armies - 1));
                        while (WhoWinsOnDefend(gameState, northAfrica.Armies + j, visibleArmies, gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) != BattleRegions.Defender)
                        {
                            j++;
                        }
                        if (j == 0 && gameState.VisibleMap.GetSuperRegion((int)SuperRegionNames.NorthAmerica).OwnedByPlayer() != me)
                        {
                            var nextRegionToUse = gameState.MyRegions.FirstOrDefault(my => my.NeutralNeighbors().Any(n => n.SuperRegion.Id == (int)SuperRegionNames.NorthAmerica));
                            if (nextRegionToUse == null) //there's an opponent there. Need to clear him out first!!
                            {
                                Console.Error.WriteLine("Round: {0}: Something didn't fire correctly trying to execute win condition 2.2", gameState.RoundNumber);
                                return armiesLeft;
                            }
                            _winConditionMet = true;
                            var nextRegionToTake = nextRegionToUse.NeutralNeighbors().First(n => n.SuperRegion.Id == (int)SuperRegionNames.NorthAmerica);
                            placeArmiesMoves[nextRegionToUse] += 3;
                            nextRegionToUse.Armies += 3;
                            armiesLeft -= 3;
                            _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, nextRegionToUse, nextRegionToTake, nextRegionToUse.Armies - 1));
                            nextRegionToUse.Armies = 1;
                            //northAfrica.Armies = 1;
                            return armiesLeft;
                        }

                        placeArmiesMoves[northAfrica] += armiesLeft;
                        northAfrica.Armies += armiesLeft;
                        foreach (var friendly in northAfrica.FriendlyNeighbors().Where(fn => fn.Armies > 1))
                        {
                            //if my big fatty stack hasn't made it to Middle East yet, then North Africa is in trouble
                            _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, friendly, northAfrica,
                                friendly.Armies - 1));
                            friendly.Armies = 1;
                            //also, if I hit this, don't let North Africa Transfer anywhere yet (might try to transfer thinking it'll help)
                            //set this if there is a stalemate with any region
                        }
                        if (
                            WhoWinsOnAttack(gameState, northAfrica.Armies, visibleArmies,
                                gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender &&
                            WhoWinsOnDefend(gameState, northAfrica.Armies + gameState.StartingArmies, visibleArmies,
                                gameState.Opponent.EstimatedIncome, _opponentMovesGuessed) == BattleRegions.Defender)
                        {
                            northAfrica.Armies = 1;
                        }
                        return 0;
                    }
                }
            }
            _winConditionMet = false;
            return armiesLeft;
        }

        private void IncrementNeighbors(BotState gameState, int region, int moveNumber)
        {
            foreach (var neighbor in gameState.VisibleMap.GetRegion(region).Neighbors.Where(n => n.MovesToOpponent > moveNumber))
            {
                neighbor.MovesToOpponent = moveNumber;
                IncrementNeighbors(gameState, neighbor.Id, moveNumber + 1);
            }
        }

        public List<PlaceArmiesMove> GetPlaceArmiesMoves(BotState gameState, long timeOut)
        {
            _myTurnMoves = new MyTurnMoves();
            //var currentSuperRegions = gameState.MyRegions.Select(m => m.SuperRegion).Distinct().ToList();
            var armiesLeft = gameState.StartingArmies;
            var opponent = gameState.Opponent;
            var placeArmiesMoves = gameState.MyRegions.ToDictionary(region => region, region => 0);
            if (gameState.RoundNumber == 1)
            {
                armiesLeft = CheckPicksForPossibleSabotage(gameState, placeArmiesMoves, armiesLeft);
            }
            //if (gameState.RoundNumber < 5)
            //{
            //    armiesLeft = CheckForBullshitTakeover(gameState, placeArmiesMoves, armiesLeft);
            //}
            _opponentMovesGuessed = GuessOpponentPlacement(gameState, opponent.EstimatedIncome);
            
            armiesLeft = CheckForWinCondition(gameState, placeArmiesMoves, armiesLeft);
            
            if (!_winConditionMet)
            {
                armiesLeft = CheckForStaleMate(gameState, placeArmiesMoves, armiesLeft);
            }
            armiesLeft = PlaceStrategicDefensiveArmies(gameState, placeArmiesMoves, armiesLeft);
            if (armiesLeft > 0)
            {
                armiesLeft = PlaceAggressiveArmies(gameState, placeArmiesMoves, armiesLeft);
            }

            armiesLeft = CompleteSuperRegions(gameState, placeArmiesMoves, armiesLeft);

            if (armiesLeft > 0)
            {
                armiesLeft = HoldUpASingleEnemy(gameState, placeArmiesMoves, armiesLeft);
            }

            if (gameState.VisibleMap.SuperRegions.Any(sr => sr.OwnedByPlayer() == gameState.MyPlayerName) && gameState.MyRegions.All(my => my.NeighboringEnemies(opponent.PlayerName).Count == 0))
            {
                armiesLeft = SeekOutOpponent(gameState, placeArmiesMoves, armiesLeft);
            }

            if (armiesLeft > 0)
            {
                armiesLeft = AttackNeighboringSuper(gameState, placeArmiesMoves, armiesLeft);
            }

            if (armiesLeft > 0)
            {
                var mySuperToTake = gameState.MyRegions.Select(my => my.SuperRegion).OrderBy(sr => sr.Ranking).FirstOrDefault(sr => sr.OwnedByPlayer() != gameState.MyPlayerName && !sr.SetToBeConquered);
                if (mySuperToTake != null)
                {
                    var regionToUse = mySuperToTake.SubRegions.OrderByDescending(r => r.Neighbors.Where(n => n.SuperRegion.Id == mySuperToTake.Id).Count(n => n.PlayerName != gameState.MyPlayerName)).FirstOrDefault(r => r.PlayerName == gameState.MyPlayerName);

                    if (regionToUse != null)
                    {
                        placeArmiesMoves[regionToUse] += armiesLeft;
                        regionToUse.Armies += armiesLeft;
                        armiesLeft = 0;
                    }
                }
            }

            //before removing a super region, see if I have a stack big enough to start approaching an opponent for reinforcement
            CheckForLargeStack(gameState);
            //remove the extra fluffy regions I do not care about
            if (gameState.MyRegions.Select(m => m.SuperRegion).Distinct().ToList().Count > 1)
            {
                foreach (var superRegion in gameState.VisibleMap.SuperRegions.OrderByDescending(sr => sr.Ranking))
                {
                    if (superRegion.KeepVisible)
                        continue;
                    
                    if (superRegion.OwnedByPlayer() == gameState.MyPlayerName || superRegion.SetToBeConquered)
                        continue;

                    var unownedSupers = gameState.VisibleMap.SuperRegions.Where(sr => sr.OwnedByPlayer() != gameState.MyPlayerName && !sr.SetToBeConquered).ToList();
                    var myNextUnownedSupers = gameState.MyRegions.Select(m => m.SuperRegion).Distinct().Where(csr => csr.OwnedByPlayer() != gameState.MyPlayerName).ToList();
                    if (myNextUnownedSupers.Any())
                    {
                        var myNextUnownedSuper = myNextUnownedSupers.First(sr => sr.Ranking == myNextUnownedSupers.Min(csr => csr.Ranking));

                        if (superRegion.Ranking == myNextUnownedSuper.Ranking)
                        {
                            if (superRegion.Ranking == unownedSupers.Max(sr => sr.Ranking) && gameState.MyRegions.Select(m => m.SuperRegion).Distinct().Count() < 3)
                            {
                                //bad! Look for lower subregion rankings to transfer to!
                                foreach (var region in myNextUnownedSuper.SubRegions.Where(sr => sr.PlayerName == gameState.MyPlayerName).ToList())
                                {
                                    var localSuper = superRegion;
                                    foreach (var neighbor in region.NeutralNeighbors().Where(n => n.SuperRegion.Id == localSuper.Id && n.SubRegionRanking > region.SubRegionRanking))
                                    {
                                        neighbor.MakeMeInvisible(true);
                                    }
                                }
                                foreach (var superNeighbor in superRegion.Neighbors)
                                    superNeighbor.KeepVisible = true;

                            }
                            continue;
                        }

                    }
                    if (superRegion.Ranking == unownedSupers.Single(us => us.Ranking == unownedSupers.Min(s => s.Ranking)).Ranking)
                        continue;

                    if (superRegion.ArmiesNeededToConquer <= gameState.MyRegions.Where(my => my.SuperRegion.Id == superRegion.Id).Sum(my => (my.Armies - 1)) && superRegion.SubRegions.All(sr => !sr.OwnedByPlayer(opponent.PlayerName)))
                        continue;

                    foreach (var region in superRegion.SubRegions.Where(sr => sr.PlayerName == gameState.MyPlayerName).ToList())
                    {
                        var localSuper = superRegion;
                        foreach (var neighbor in region.NeutralNeighbors().Where(n => n.SuperRegion.Id == localSuper.Id))
                        {
                            neighbor.MakeMeInvisible(true);
                        }
                    }

                    if (superRegion.SubRegions.All(sr => sr.PlayerName != gameState.MyPlayerName) && superRegion.SubRegions.All(sr => sr.PlayerName != opponent.PlayerName))
                    {
                        foreach (var region in superRegion.SubRegions.Where(sr => !sr.OwnedByPlayer(gameState.Opponent.PlayerName)))
                        {
                            region.MakeMeInvisible(true);
                        }
                    }
                }
            }
            if (armiesLeft > 0)
            {
                //find the region closest to North Africa and put the rest there.  If there's a closer move to an opponent, put it there instead.
                if (gameState.VisibleMap.GetRegion((int)RegionNames.NorthAfrica).PlayerName == Constants.UNKNOWN) 
                { 
                    var northAfrica = gameState.VisibleMap.GetRegion((int)RegionNames.NorthAfrica);
                    northAfrica.MovesToOpponent = 0;
                    IncrementNeighbors(gameState, northAfrica.Id, 1);
                }
                var closestRegion = gameState.MyRegions.First(my => my.MovesToOpponent == gameState.MyRegions.Min(r => r.MovesToOpponent));
                placeArmiesMoves[closestRegion] += armiesLeft;
                closestRegion.Armies += armiesLeft;
            }
            _lastOpposingRegionCount = opponent.Regions.Count(r => r.NeighboringEnemies(gameState.MyPlayerName).Any());
            if (placeArmiesMoves.Count(p => p.Value > 0) == 1)
                _singlePlaceArmyMoveThisRound = true;
            _regroupArmies = false; //any regrouping needs to happen in logic prior to this
            return placeArmiesMoves.Where(p => p.Value > 0).Select(placement => new PlaceArmiesMove(gameState.MyPlayerName, placement.Key, placement.Value)).ToList();
        }

        public List<AttackTransferMove> GetAttackTransferMoves(BotState gameState, long timeOut)
        {
            var priorityAttackPlaced = false;
            var opponent = gameState.Opponent;
            var me = gameState.MyPlayerName;

            var possibleFromRegions = gameState.MyRegions.Where(m => m.Armies > 1).OrderByDescending(m => m.Armies).ToList();
            var opponentStrengthOffset = _opponentMovesGuessed ? 0 : opponent.EstimatedIncome;

            foreach (var fromRegion in possibleFromRegions)
            {
                fromRegion.ArmiesLeft = fromRegion.Armies - 1;
                var possibleToRegions = new List<Region>();

                if (fromRegion.SuperRegion.Id == (int)SuperRegionNames.Asia)
                {
                    if (gameState.StartingArmies > opponent.EstimatedIncome)
                    {
                        possibleToRegions.AddRange(fromRegion.NeighboringEnemies(opponent.PlayerName).OrderBy(r => r.Armies));
                    }
                    else if (fromRegion.NeighboringEnemies(opponent.PlayerName).All(op => op.Armies >= 3))
                    {
                        if(fromRegion.NeutralNeighbors().All(n => n.SubRegionRanking >= fromRegion.SubRegionRanking))
                            possibleToRegions.AddRange(fromRegion.NeighboringEnemies(opponent.PlayerName).OrderBy(r => r.Armies));
                    }
                    possibleToRegions.AddRange(fromRegion.NeutralNeighbors().OrderBy(r => r.MovesToOpponent));
                }
                else
                {
                    possibleToRegions.AddRange(fromRegion.NeighboringEnemies(opponent.PlayerName).OrderBy(r => r.SubRegionRanking));
                    possibleToRegions.AddRange(fromRegion.NeutralNeighbors().OrderByDescending(r => r.SubRegionRanking).ThenByDescending(r => r.MovesToOpponent));
                }

                if (possibleToRegions.All(p => p.OwnedByPlayer(Constants.NEUTRAL_PLAYER)) && !fromRegion.HasAttacked(_myTurnMoves))
                {
                    if (fromRegion.NeutralNeighbors().Any(n => n.SubRegionRanking < fromRegion.SubRegionRanking))
                    {
                        if (fromRegion.SuperRegion.Ranking == gameState.MyRegions.Max(my => my.SuperRegion.Ranking))
                        {
                            if(!_winConditionMet)
                                if (gameState.StartingArmies <= opponent.EstimatedIncome && fromRegion.Armies < 20 && fromRegion.Neighbors.All(n => n.SuperRegion.SubRegions.Any(sr => sr.PlayerName == opponent.PlayerName)))
                                    continue;
                        }
                        var toRegion = _winConditionMet ? fromRegion.NeutralNeighbors().FirstOrDefault(r => r.SuperRegion.Id == fromRegion.SuperRegion.Id) : fromRegion.NeutralNeighbors().FirstOrDefault(r => r.SubRegionRanking == fromRegion.NeutralNeighbors().Min(nn => nn.SubRegionRanking));
                        if (toRegion != null && fromRegion.ArmiesLeft > toRegion.Armies + 1)
                        {
                            _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                            fromRegion.ArmiesLeft = 0;
                        }
                    }
                }

                var opponentDefensiveStrength = fromRegion.NeighboringEnemies(opponent.PlayerName).Sum(o => o.Armies) + opponentStrengthOffset;
                if (fromRegion.Neighbors.All(n => n.OwnedByPlayer(me)))
                {
                    //No enemies around, move closer to one here
                    var toRegion = fromRegion.FriendlyNeighbors().FirstOrDefault(fn => fn.MovesToOpponent == fromRegion.FriendlyNeighbors().Min(f => f.MovesToOpponent));
                    if (toRegion != null && toRegion.MovesToOpponent < fromRegion.MovesToOpponent && !fromRegion.StackHere)
                    {
                        _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                        fromRegion.ArmiesLeft = 0;
                    }

                }
                if (fromRegion.ArmiesLeft <= 0)
                    continue;

                foreach (var toRegion in possibleToRegions)
                {
                    if (fromRegion.ArmiesLeft == 0)
                        break;

                    if (toRegion.PlayerName == opponent.PlayerName)
                    {
                        toRegion.Armies = toRegion.Armies + opponentStrengthOffset;

                        var strengthAgainstTo = toRegion.Neighbors.Where(n => n.PlayerName == me).Sum(mine => mine.Armies - 1);
                        if (fromRegion.ArmiesLeft > toRegion.Armies && WhoWinsOnAttack(gameState, fromRegion.ArmiesLeft + 1, toRegion.Armies - opponentStrengthOffset, opponent.EstimatedIncome, _opponentMovesGuessed || opponentStrengthOffset == 0) == BattleRegions.Attacker)
                        {
                            if (fromRegion.Armies > 2)
                            {
                                if (opponentDefensiveStrength == toRegion.Armies)
                                {
                                    if (toRegion.NeighboringEnemies(me).Count == 2)
                                    {
                                        if (toRegion.NeighboringEnemies(me).Any(my => my.Armies < (toRegion.Armies - 1) / 2))
                                        {
                                            //let's do a sneaky transfer instead
                                            var regionToTransfer = toRegion.NeighboringEnemies(me).First(my => my.Armies == toRegion.NeighboringEnemies(me).Min(n => n.Armies));
                                            Console.Error.WriteLine("Round {0}: Maybe a transfer from {1}: {2} to {3}: {4}?", gameState.RoundNumber, fromRegion.Id, fromRegion.Name, regionToTransfer.Id, regionToTransfer.Name);
                                        }
                                    }
                                    if (fromRegion.SuperRegion.Id == (int)SuperRegionNames.Asia && toRegion.SuperRegion.Id == (int)SuperRegionNames.Africa && _opponentMovesGuessed)
                                    {
                                        //does a neutral region look better?
                                        foreach (var neutralRegion in possibleToRegions.Where(p => p.OwnedByPlayer(Constants.NEUTRAL_PLAYER) && p.SubRegionRanking < fromRegion.SubRegionRanking))
                                        {
                                            if (toRegion.Armies > neutralRegion.Armies)
                                            {
                                                _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, neutralRegion, fromRegion.ArmiesLeft));
                                                fromRegion.ArmiesLeft = 0;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (fromRegion.NeutralNeighbors().Any(n => n.SubRegionRanking < toRegion.SubRegionRanking))
                                        {
                                             if(fromRegion.SuperRegion.Id == (int)SuperRegionNames.Africa && !_winConditionMet)
                                                if(gameState.StartingArmies <= opponent.EstimatedIncome)
                                                    continue;
                                        }
                                        _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                                        if (toRegion.NeighboringEnemies(me).Count == 1)
                                            toRegion.MakeMeInvisible();
                                        var expectedArmiesLeft = DefendingArmiesLeft(fromRegion.ArmiesLeft, toRegion.Armies);
                                        fromRegion.ArmiesLeft = 0;
                                        toRegion.Armies = (int)(expectedArmiesLeft + .5);
                                        if (toRegion.Armies < 0) toRegion.Armies = 0;
                                        opponentStrengthOffset = 0;
                                    }

                                }
                                else if ((double)fromRegion.ArmiesLeft / opponentDefensiveStrength > 1.5)
                                {
                                    if (fromRegion.SuperRegion.Ranking > toRegion.SuperRegion.Ranking + 2)
                                    {
                                        _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                                        fromRegion.ArmiesLeft = 0;
                                    }
                                    else
                                    {
                                        _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft >= toRegion.Armies * 2 ? toRegion.Armies * 2 : fromRegion.ArmiesLeft));
                                        fromRegion.ArmiesLeft -= (fromRegion.ArmiesLeft >= toRegion.Armies * 2 ? toRegion.Armies * 2 : fromRegion.ArmiesLeft);
                                    }
                                    toRegion.Armies -= opponentStrengthOffset;
                                    opponentStrengthOffset = 0;
                                }
                                else if (fromRegion.SuperRegion.OwnedByPlayer() != me && (fromRegion.Id != (int)RegionNames.NorthAfrica && !_winConditionMet))  //I've got nothing to lose if this stack is big enough
                                {
                                    if (!fromRegion.StrategicDefenseRegion)
                                    {
                                        if (toRegion.SubRegionRanking < fromRegion.SubRegionRanking)
                                        {
                                            Console.Error.WriteLine("Round {0}: I see an opening with a better army placement at {1}: {2}.  Attempting to conquer with {3}: {4}", gameState.RoundNumber, toRegion.Id, toRegion.Name, fromRegion.Id, fromRegion.Name);
                                            _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                                            fromRegion.ArmiesLeft = 0;
                                        }
                                        else
                                        {
                                            if (possibleToRegions.Any(r => opponent.OwnedSuperRegions.Select(sr => sr.Id).Distinct().Contains(r.SuperRegion.Id)))
                                            {
                                                if (!opponent.OwnedSuperRegions.Select(sr => sr.Id).Distinct().Contains(toRegion.SuperRegion.Id))
                                                {
                                                    continue;
                                                }
                                            }
                                            if (fromRegion.NeutralNeighbors().Any(n => n.SubRegionRanking < fromRegion.SubRegionRanking) && fromRegion.SuperRegion.Id == (int)SuperRegionNames.Africa)
                                            {
                                                continue;
                                            }
                                            Console.Error.WriteLine("Round {0}: I see an opening at {1}: {2}.  Attempting to conquer with {3}: {4}", gameState.RoundNumber, toRegion.Id, toRegion.Name, fromRegion.Id, fromRegion.Name);
                                            _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft >= toRegion.Armies * 2 ? toRegion.Armies * 2 : fromRegion.ArmiesLeft));
                                            fromRegion.ArmiesLeft -= (fromRegion.ArmiesLeft >= toRegion.Armies * 2 ? toRegion.Armies * 2 : fromRegion.ArmiesLeft);
                                            opponentStrengthOffset = 0;
                                        }
                                    }
                                    else if (fromRegion.ArmiesLeft > toRegion.Armies && gameState.StartingArmies > opponent.EstimatedIncome)
                                    {
                                        _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, (toRegion.Armies - opponentStrengthOffset) * 2));
                                        fromRegion.ArmiesLeft -= (toRegion.Armies - opponentStrengthOffset) * 2;
                                    }
                                }
                                else if (WhoWinsOnAttack(gameState, fromRegion.ArmiesLeft + 1, toRegion.Armies, opponent.EstimatedIncome, _opponentMovesGuessed || opponentStrengthOffset == 0) == BattleRegions.Attacker)
                                {
                                    if(_winConditionMet && fromRegion.Id == (int)RegionNames.NorthAfrica)
                                        if (toRegion.Armies == 1)
                                            continue;
                                    
                                    var otherArmies = 0.0;
                                    var maxArmiesToAttack = fromRegion.ArmiesLeft > toRegion.Armies * 2 ? toRegion.Armies * 2 : fromRegion.ArmiesLeft;
                                    double armiesLeft = fromRegion.Armies - maxArmiesToAttack;
                                    foreach (var neighbor in fromRegion.NeighboringEnemies(opponent.PlayerName).Where(n => n.Id != toRegion.Id).OrderByDescending(n => n.Armies))
                                    {
                                        armiesLeft = DefendingArmiesLeft(neighbor.Armies - 1, (int)armiesLeft);
                                        var opArmiesLeft = AttackingArmiesLeft(neighbor.Armies - 1, (int)armiesLeft);
                                        otherArmies += opArmiesLeft > 0 ? opArmiesLeft : 0;
                                        if (armiesLeft <= 0) break;
                                    }
                                    
                                    if (armiesLeft > 1 && WhoWinsOnDefend(gameState, (int)armiesLeft + gameState.StartingArmies, (int)(otherArmies + .5), opponent.EstimatedIncome, false) == BattleRegions.Defender)
                                    {
                                        if (toRegion.SubRegionRanking < fromRegion.SubRegionRanking || (_winConditionMet && fromRegion.Id == (int)RegionNames.NorthAfrica && toRegion.Id == (int)RegionNames.Brazil))
                                        {
                                            _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                                            fromRegion.ArmiesLeft = 0;
                                        }
                                        else
                                        {
                                            _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, toRegion.Armies * 2));
                                            fromRegion.ArmiesLeft -= toRegion.Armies * 2;
                                            if (_winConditionMet)
                                                _regroupArmies = true;
                                        }                                        
                                    }
                                }
                            }
                            else
                            {
                                fromRegion.StrategicDefenseRegion = true;
                                fromRegion.StrategicDefensePriority = 3;
                                fromRegion.StackHere = true;
                            }
                        }
                        else if (fromRegion.Armies < toRegion.Armies - 1 && WhoWinsOnDefend(gameState, fromRegion.Armies, toRegion.Armies - opponentStrengthOffset - 1, opponent.EstimatedIncome, _opponentMovesGuessed || opponentStrengthOffset == 0) == BattleRegions.Attacker)
                        {
                            if (gameState.MyRegions.Select(my => my.SuperRegion).Any(sr => sr.SetToBeConquered))
                            {
                                //do an early game sanity check
                                //basically, if I'm about to finish up a super region and the difference between armies is less that what I expect
                                //to gain next turn, hold out.
                                var finishingSupers = gameState.MyRegions.Select(my => my.SuperRegion).Distinct().Where(sr => sr.SetToBeConquered);
                                var expectedNewIncome = finishingSupers.Sum(s => s.ArmiesReward);
                                if (fromRegion.Armies + expectedNewIncome >= toRegion.Armies - 1)
                                {
                                    continue;
                                }
                            }
                            if (fromRegion.SuperRegion.OwnedByPlayer() != me)
                            {
                                //need to remember that friendly transfers need to happen. This is only in case of emergency and I don't want opponent to take super region yet
                                //i.e. I just took opponent out of Australia and now he's got his eye on South America where I have armies
                                if (fromRegion.ArmiesLeft >= 3 && fromRegion.FriendlyNeighbors().Count == 0 && opponent.OwnedSuperRegions.Count > 0)
                                {
                                    var neutralToConquer = fromRegion.NeutralNeighbors().FirstOrDefault(nn => nn.SuperRegion.Id == fromRegion.SuperRegion.Id);

                                    if (neutralToConquer == null)
                                    {
                                        foreach (var neighbor in gameState.VisibleMap.Regions.Where(r => r.IsNeighbor(fromRegion)).OrderByDescending(n => n.SuperRegion.Ranking).ThenByDescending(n => n.SubRegionRanking))
                                        {
                                            if (!fromRegion.Neighbors.Contains(neighbor))
                                            {
                                                neutralToConquer = neighbor;
                                            }
                                        }
                                    }
                                    if (neutralToConquer != null)
                                    {
                                        Console.Error.WriteLine("Round {0}: Potential to prevent opponent from taking {1} with {2}: {3}", gameState.RoundNumber, fromRegion.SuperRegion.Name, fromRegion.Id, fromRegion.Name);
                                        //put that shit as your FIRST move
                                        _myTurnMoves.FirstTransferMoves.Insert(0, new AttackTransferMove(me, fromRegion, neutralToConquer, fromRegion.ArmiesLeft));
                                        fromRegion.ArmiesLeft = 0;
                                        priorityAttackPlaced = true;
                                    }
                                }

                            }
                        }
                        else if ((double)strengthAgainstTo / toRegion.Armies > 2.5 && fromRegion.ArmiesLeft > toRegion.Armies)
                        {
                            if (fromRegion.ArmiesLeft > toRegion.Armies * 2)
                            {
                                _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, toRegion.Armies * 2));
                                fromRegion.ArmiesLeft -= toRegion.Armies * 2;
                            }
                            else
                            {
                                _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                                fromRegion.ArmiesLeft = 0;
                            }
                        }
                        else if (fromRegion.FriendlyNeighbors().Any(fn => fn.StrategicDefenseRegion))
                        {
                            if (fromRegion.StrategicDefenseRegion)
                            {
                                if (fromRegion.FriendlyNeighbors().Any(fn => fn.StrategicDefensePriority < fromRegion.StrategicDefensePriority))
                                    continue;
                            }
                        }
                        else
                        {
                            if (opponent.EstimatedIncome > gameState.StartingArmies && opponent.EstimatedIncome > 7)
                            {
                                if (fromRegion.Armies == gameState.MyRegions.Max(my => my.Armies))
                                {
                                    foreach (var region in gameState.VisibleMap.Regions)
                                    {
                                        region.MovesToOpponent = int.MaxValue;
                                        if (gameState.FullMap.GetRegion(region.Id).IsNeighbor(gameState.FullMap.GetRegion(fromRegion.Id)))
                                            fromRegion.AddNeighbor(region);
                                    }
                                    var superToBreak = opponent.OwnedSuperRegions.OrderByDescending(super => super.ArmiesReward).First();
                                    foreach (var region in superToBreak.SubRegions.Where(sr => sr.Neighbors.Any(n => n.SuperRegion != sr.SuperRegion && opponent.OwnedSuperRegions.All(o => o.Id != n.SuperRegion.Id))))
                                    {
                                        region.MovesToOpponent = 0;
                                        IncrementNeighbors(gameState, region.Id, 1);
                                    }

                                    var attackableNeighbors = fromRegion.NeutralNeighbors();
                                    var attackHere = attackableNeighbors.FirstOrDefault(an => an.MovesToOpponent == attackableNeighbors.Min(n => n.MovesToOpponent));
                                    if (attackHere != null && attackHere.MovesToOpponent < fromRegion.MovesToOpponent)
                                    {
                                        _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, fromRegion, attackHere, fromRegion.ArmiesLeft));
                                        fromRegion.ArmiesLeft = 0;
                                    }
                                    else 
                                    {
                                        //hail mary
                                        attackableNeighbors = fromRegion.NeighboringEnemies(opponent.PlayerName).OrderBy(o => o.SubRegionRanking).ToList();
                                        foreach (var neighbor in attackableNeighbors)
                                        {
                                            if (fromRegion.ArmiesLeft > neighbor.Armies + opponentStrengthOffset)
                                            {
                                                _myTurnMoves.LastAttackMoves.AddAttack(new AttackTransferMove(me, fromRegion, neighbor, fromRegion.ArmiesLeft));
                                                fromRegion.ArmiesLeft = 0;
                                            }
                                        }
                                        if (fromRegion.Armies > 0)
                                        {
                                            if (attackHere != null)
                                            {
                                                //gotta do something!
                                                _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, fromRegion, attackHere, fromRegion.ArmiesLeft));
                                                fromRegion.ArmiesLeft = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var i = 0;
                                while (WhoWinsOnDefend(gameState, fromRegion.ArmiesLeft + 1 - i, opponentDefensiveStrength - opponentStrengthOffset - fromRegion.NeighboringEnemies(opponent.PlayerName).Count, opponent.EstimatedIncome, _opponentMovesGuessed || opponentStrengthOffset == 0) == BattleRegions.Defender)
                                {
                                    i++;
                                }
                                if (i > 0)
                                {
                                    Console.Error.WriteLine("Round {0}: Potentially {1} armies available on {2} for seeking new income", gameState.RoundNumber, i, fromRegion.Name);

                                    var fromRegionSuper = fromRegion.SuperRegion;
                                    var enemiesInRegion = fromRegion.NeighboringEnemies(opponent.PlayerName).Where(o => o.SuperRegion == fromRegionSuper && o.IsNeighbor(fromRegion)).OrderByDescending(o => o.Armies).ToList();

                                    if (enemiesInRegion.Any())
                                    {
                                        foreach (var visibleNeighbor in enemiesInRegion)
                                        {
                                            if (i - 1 >= 2 * visibleNeighbor.Armies)
                                            {
                                                if (visibleNeighbor.IsNeighbor(fromRegion))
                                                {
                                                    _myTurnMoves.LastAttackMoves.Add(new AttackTransferMove(me, fromRegion, visibleNeighbor, i - 1));
                                                    fromRegion.ArmiesLeft -= i - 1;
                                                    _regroupArmies = true;
                                                }
                                                else
                                                {
                                                    var transferRegion = gameState.MyRegions.FirstOrDefault(friendly => visibleNeighbor.IsNeighbor(friendly) && fromRegion.IsNeighbor(friendly)) ?? fromRegion.NeutralNeighbors().FirstOrDefault(nn => visibleNeighbor.IsNeighbor(nn) && fromRegion.IsNeighbor(nn));
                                                    if (transferRegion != null)
                                                    {
                                                        _myTurnMoves.LastAttackMoves.Add(new AttackTransferMove(me, fromRegion, transferRegion, i - 1));
                                                        fromRegion.ArmiesLeft -= i;
                                                        _regroupArmies = true;
                                                    }
                                                }
                                                i = 0;
                                            }
                                            else
                                            {
                                                break;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        var regionsToConquer = fromRegionSuper.SubRegions.Where(sr => sr.PlayerName != me).ToList();
                                        if (regionsToConquer.Any(r => r.IsNeighbor(fromRegion)))
                                        {
                                            var armiesNeededToConquer = regionsToConquer.Count() * 3;
                                            if (i - 1 >= armiesNeededToConquer)
                                            {
                                                //use first or default because completesuperregion might make the region I want to attack invisible
                                                var regionToAttack = fromRegion.NeutralNeighbors().FirstOrDefault(n => n.SubRegionRanking == fromRegion.NeutralNeighbors().Min(ne => ne.SubRegionRanking) && n.SuperRegion.Id == fromRegion.SuperRegion.Id);
                                                if (regionToAttack != null)
                                                {
                                                    _myTurnMoves.LastAttackMoves.Add(new AttackTransferMove(me, fromRegion, regionToAttack, armiesNeededToConquer));
                                                    fromRegion.ArmiesLeft -= armiesNeededToConquer;
                                                    //no need to regroup armies here as north africa has enough armies to defend and this stack
                                                    //can come to the rescue if NA is in trouble.
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                            fromRegion.StackHere = true;
                        }
                        toRegion.Armies -= opponentStrengthOffset;
                    }
                    else if (toRegion.PlayerName == Constants.NEUTRAL_PLAYER && !fromRegion.StrategicDefenseRegion)
                    {
                        if (!fromRegion.NeighboringEnemies(opponent.PlayerName).Any() && fromRegion.ArmiesLeft >= toRegion.Armies + 1)
                        {
                            //try to complete a bonus before attacking neutrals in new territories
                            if ((fromRegion.SuperRegion.OwnedByPlayer() == me || fromRegion.SuperRegion.SetToBeConquered) && !fromRegion.SuperRegion.SubRegions.Contains(toRegion))
                            {
                                if(fromRegion.SuperRegion.SetToBeConquered && _myTurnMoves.AttackTransferMoves.Any(a => a.FromRegion.Id == fromRegion.Id))
                                {
                                    bool usedToCompleteSuper = false;
                                    foreach (var move in _myTurnMoves.AttackTransferMoves)
                                    {
                                        if (move.FromRegion.Id == fromRegion.Id)
                                        {
                                            if (move.ToRegion.SuperRegion.Id == fromRegion.SuperRegion.Id)
                                                usedToCompleteSuper = true;
                                        }
                                    }
                                    if(usedToCompleteSuper) continue;
                                }
                                if (fromRegion.Neighbors.Count - fromRegion.FriendlyNeighbors().Count == 1)
                                {
                                    //if I control all surrounding regions and am about to attack a new territory, send all troops, not just 3
                                    if (fromRegion.Neighbors.All(n => !n.StrategicDefenseRegion))
                                    {
                                        _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                                        toRegion.MakeMeInvisible();
                                        fromRegion.ArmiesLeft = 0;
                                    }
                                    else
                                    {
                                        foreach (var neighbor in fromRegion.FriendlyNeighbors())
                                        {
                                            if (neighbor.NeighboringEnemies(gameState.Opponent.PlayerName).Any())
                                                break;

                                        }
                                        Console.Error.WriteLine(
                                            "Round {0}: {1}: {2} has a neighbor that's a strategic defense region. That region has no enemy territory neighbors.  Attacking {3}: {4}",
                                            gameState.RoundNumber, fromRegion.Id, fromRegion.Name, toRegion.Id, toRegion.Name);

                                        _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, 3));
                                        toRegion.MakeMeInvisible();
                                        fromRegion.ArmiesLeft -= 3;
                                    }
                                }
                                else
                                {
                                    if (fromRegion.FriendlyNeighbors().All(fn => !fn.StrategicDefenseRegion))
                                    {
                                        _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, 3));
                                        toRegion.MakeMeInvisible();
                                        fromRegion.ArmiesLeft -= 3;
                                    }
                                }
                            }
                            else if (fromRegion.SuperRegion.OwnedByPlayer() != me && fromRegion.SuperRegion.SubRegions.Contains(toRegion) || toRegion.SuperRegion.Ranking < fromRegion.SuperRegion.Ranking)
                            {
                                if (!fromRegion.SuperRegion.SetToBeConquered)
                                {
                                    if (fromRegion.NeutralNeighbors().Count(n => n.SuperRegion == fromRegion.SuperRegion && n.SubRegionRanking <= fromRegion.SubRegionRanking) == 1)
                                    {
                                        var subRegionPriority = fromRegion.NeutralNeighbors().Single(n => n.SuperRegion == fromRegion.SuperRegion && n.SubRegionRanking <= fromRegion.SubRegionRanking);

                                        _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, subRegionPriority, fromRegion.ArmiesLeft));
                                        fromRegion.ArmiesLeft = 0;
                                    }
                                    else if (fromRegion.NeutralNeighbors().Count > 1)
                                    {
                                        if (fromRegion.ArmiesLeft < toRegion.Armies + 1)
                                            continue;
                                        _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, toRegion.Armies + 1));
                                        toRegion.MakeMeInvisible();
                                        fromRegion.ArmiesLeft -= toRegion.Armies + 1;
                                    }
                                    else
                                    {
                                        _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                                        toRegion.MakeMeInvisible();
                                        fromRegion.ArmiesLeft = 0;
                                    }
                                }
                            }
                            else if (toRegion.SuperRegion.SubRegions.Any(sr => sr.OwnedByPlayer(me)))
                            {
                                _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, 3));
                                toRegion.MakeMeInvisible();
                                fromRegion.ArmiesLeft -= 3;
                            }
                            else if (!fromRegion.SuperRegion.SubRegions.Any(sr => sr.OwnedByPlayer(Constants.NEUTRAL_PLAYER)))
                            {
                                if (fromRegion.Neighbors.All(n => n.NeighboringEnemies(opponent.PlayerName).Count == 0))
                                {
                                    _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                                    toRegion.MakeMeInvisible();
                                    fromRegion.ArmiesLeft = 0;
                                }
                            }
                        }
                        else if (fromRegion.NeighboringEnemies(opponent.PlayerName).Any() && fromRegion.SuperRegion.OwnedByPlayer() != gameState.MyPlayerName)
                        {
                            if (toRegion.SubRegionRanking < fromRegion.SubRegionRanking && toRegion.SubRegionRanking == fromRegion.NeutralNeighbors().Min(nn => nn.SubRegionRanking) && fromRegion.ArmiesLeft >= toRegion.Armies + 1)
                            {
                                //if there is a better ranked neutral region to hit AND the opponent doesn't control the best position of the region, then hit it
                                if (fromRegion.NeighboringEnemies(opponent.PlayerName).All(o => o.SubRegionRanking != fromRegion.SuperRegion.SubRegions.Min(sr => sr.SubRegionRanking)))
                                {
                                    _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, toRegion, fromRegion.ArmiesLeft));
                                    fromRegion.ArmiesLeft = 0;
                                }
                            }
                            if (opponent.EstimatedIncome > gameState.StartingArmies && opponent.EstimatedIncome > 7)
                            {
                                if (fromRegion.Armies == gameState.MyRegions.Max(my => my.Armies))
                                {
                                    foreach (var region in gameState.VisibleMap.Regions)
                                    {
                                        region.MovesToOpponent = int.MaxValue;
                                        if (gameState.FullMap.GetRegion(region.Id).IsNeighbor(gameState.FullMap.GetRegion(fromRegion.Id)))
                                            fromRegion.AddNeighbor(region);
                                    }
                                    var superToBreak = opponent.OwnedSuperRegions.OrderByDescending(super => super.ArmiesReward).First();
                                    foreach (var region in superToBreak.SubRegions.Where(sr => sr.Neighbors.Any(n => n.SuperRegion != sr.SuperRegion && opponent.OwnedSuperRegions.All(o => o.Id != n.SuperRegion.Id))))
                                    {
                                        region.MovesToOpponent = 0;
                                        IncrementNeighbors(gameState, region.Id, 1);
                                    }

                                    var attackableNeighbors = fromRegion.NeutralNeighbors().Union(fromRegion.FriendlyNeighbors()).ToList();
                                    var attackHere = attackableNeighbors.FirstOrDefault(an => an.MovesToOpponent == attackableNeighbors.Min(n => n.MovesToOpponent));
                                    if (attackHere != null)
                                    {
                                        _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, fromRegion, attackHere, fromRegion.ArmiesLeft));
                                        fromRegion.ArmiesLeft = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                if (fromRegion.StrategicDefenseRegion)
                    fromRegion.ArmiesLeft = 0;
                if (fromRegion.ArmiesLeft == 0 || fromRegion.StackHere)
                    continue;
                var toRegions = fromRegion.Neighbors.Where(p => p.PlayerName == me).ToList();
                if (toRegions.Count > 0 && !(_winConditionMet && fromRegion.Id == (int)RegionNames.NorthAfrica))
                {
                    var friendlyNeighborsWithOpponents = toRegions.Where(t => t.Neighbors.Any(n => n.PlayerName == opponent.PlayerName)).ToList();
                    if (friendlyNeighborsWithOpponents.Count > 0)
                    {
                        var friendly = friendlyNeighborsWithOpponents.OrderByDescending(f => f.Neighbors.Where(n => n.PlayerName == opponent.PlayerName).Sum(n => n.Armies)).First();
                        if (!friendly.HasAttacked(_myTurnMoves))
                        {
                            if (WhoWinsOnDefend(gameState, friendly.Armies + fromRegion.ArmiesLeft, friendly.NeighboringEnemies(opponent.PlayerName).Sum(op => (op.Armies - 1)), opponent.EstimatedIncome, _opponentMovesGuessed || opponentStrengthOffset == 0) == BattleRegions.Defender)
                            {
                                _myTurnMoves.FirstTransferMoves.Insert(0, new AttackTransferMove(me, fromRegion, friendly, fromRegion.ArmiesLeft));
                                fromRegion.ArmiesLeft = 0;
                            }
                        }
                    }
                    else if (toRegions.Any(r => r.StackHere))
                    {
                        var friendlyTransfer = toRegions.First(r => r.StackHere);
                        _myTurnMoves.AttackTransferMoves.AddAttack(new AttackTransferMove(me, fromRegion, friendlyTransfer, fromRegion.ArmiesLeft));
                        fromRegion.ArmiesLeft = 0;
                    }
                    else
                    {
                        if (!fromRegion.StrategicPosition && !fromRegion.HasAttacked(_myTurnMoves))
                        {
                            var defaultNeighbor = toRegions.First(f => f.MovesToOpponent == toRegions.Min(to => to.MovesToOpponent));
                            if (defaultNeighbor.MovesToOpponent < fromRegion.MovesToOpponent)
                            {
                                _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, fromRegion, defaultNeighbor, fromRegion.ArmiesLeft));
                                fromRegion.ArmiesLeft = 0;
                            }
                        }
                        else if (fromRegion.Id == (int)RegionNames.Indonesia)
                        {
                            if (gameState.VisibleMap.GetSuperRegion((int)SuperRegionNames.Australia).OwnedByPlayer() != me && !fromRegion.NeutralNeighbors().Any() && gameState.StartingArmies <= opponent.EstimatedIncome)
                            {
                                //this gets hit when we don't own Australia, no opponents around. Don't stack in Indonesia yet
                                var completeSuperTransfer = toRegions.First(to => to.Armies == toRegions.Max(t => t.Armies));
                                _myTurnMoves.FirstTransferMoves.Add(new AttackTransferMove(me, fromRegion, completeSuperTransfer, fromRegion.ArmiesLeft));
                                fromRegion.ArmiesLeft = 0;
                                completeSuperTransfer.StackHere = true;
                            }
                        }
                    }
                }
            }


            for (var i = _myTurnMoves.AttackTransferMoves.Count - 1; i >= 0; i--)
            {
                var attackTransferMove = _myTurnMoves.AttackTransferMoves[i];
                var fromRegion = attackTransferMove.FromRegion;
                if (fromRegion.ArmiesLeft > 0 && !fromRegion.NeighboringEnemies(opponent.PlayerName).Any() && attackTransferMove.ToRegion.SubRegionRanking <= fromRegion.SubRegionRanking)
                {
                    Console.Error.WriteLine("Round {0}: Adding {1} remaining armies to attack from {2}: {3} to {4}: {5}", gameState.RoundNumber, fromRegion.ArmiesLeft, fromRegion.Id, fromRegion.Name, _myTurnMoves.AttackTransferMoves[i].ToRegion.Id, _myTurnMoves.AttackTransferMoves[i].ToRegion.Name);
                    _myTurnMoves.AttackTransferMoves[i].Armies += fromRegion.ArmiesLeft;
                    fromRegion.ArmiesLeft = 0;
                }
                else if (fromRegion.ArmiesLeft > 0 && !fromRegion.NeighboringEnemies(opponent.PlayerName).Any())
                {
                    //just add 1 army to the attack (make a 4 take a 2 and a 3 take a 1
                    _myTurnMoves.AttackTransferMoves[i].Armies++;
                    fromRegion.ArmiesLeft--;
                }
            }
            
            var highAttack = _myTurnMoves.LastAttackMoves.FirstOrDefault();
            if (highAttack != null && !priorityAttackPlaced)
            {
                _myTurnMoves.FirstTransferMoves.Insert(0, highAttack);
                _myTurnMoves.LastAttackMoves.Remove(highAttack);
            }
            var myMoves = _myTurnMoves.FirstTransferMoves.Union(_myTurnMoves.AttackTransferMoves.Union(_myTurnMoves.LastAttackMoves)).ToList();

            if (!_winConditionMet && _singlePlaceArmyMoveThisRound && opponent.SinglePlaceArmyThisRound)
            {
                var movesToCheck = _myTurnMoves.AttackTransferMoves.Union(_myTurnMoves.LastAttackMoves).ToList();
                if(highAttack != null)
                    movesToCheck.Add(highAttack);
                if (movesToCheck.Count == 0)
                    _possibleStaleMateRounds++;
                if (movesToCheck.Count == 1 && _staleMateMove)
                    _possibleStaleMateRounds++;

                if(_possibleStaleMateRounds > 0)
                    Console.Error.WriteLine("Round {0}: Possible stalemate for {1} rounds", gameState.RoundNumber, _possibleStaleMateRounds);
            }
            else
            {
                _possibleStaleMateRounds = 0;
            }
            _opponentMovesGuessed = false;
            return myMoves;
        }

        private double DefendingArmiesLeft(int fromRegion, int toRegion)
        {
            double attackingArmies = fromRegion;
            double defendingArmies = toRegion;
            var defendersDestroyed = attackingArmies * .6;

            defendingArmies -= defendersDestroyed;

            return defendingArmies;
        }

        private double AttackingArmiesLeft(int fromRegion, int toRegion)
        {
            double attackingArmies = fromRegion;
            double defendingArmies = toRegion;
            var attackersDestroyed = defendingArmies * .7;

            attackingArmies -= attackersDestroyed;
            return attackingArmies;
        }

        private BattleRegions WhoWinsOnAttack(BotState currentState, int fromRegionArmies, int toRegionArmies, int opponentIncome, bool noOpponentIncomeThisRound)
        {
            var attackingArmies = fromRegionArmies - 1;
            var defendingArmies = toRegionArmies + (noOpponentIncomeThisRound ? 0 : opponentIncome);
            var defendersDestroyed = attackingArmies * .6;
            var attackersDestroyed = defendingArmies * .7;

            var attackingArmiesLeft = attackingArmies - attackersDestroyed;
            var defendingArmiesLeft = defendingArmies - defendersDestroyed;
            if (attackingArmiesLeft <= 0)
                return BattleRegions.Defender;

            if (defendingArmiesLeft <= 0)
                return BattleRegions.Attacker;

            if ((int)attackingArmiesLeft + currentState.StartingArmies == fromRegionArmies && (int)defendingArmiesLeft == toRegionArmies)
                return BattleRegions.Neither;

            return WhoWinsOnAttack(currentState, (int)attackingArmiesLeft + currentState.StartingArmies, (int)defendingArmiesLeft, opponentIncome, false);
        }

        private BattleRegions WhoWinsOnDefend(BotState currentState, int fromRegionArmies, int toRegionArmies, int opponentIncome, bool noOpponentIncomeThisRound)
        {
            var attackingArmies = toRegionArmies + (noOpponentIncomeThisRound ? 0 : opponentIncome);
            var defendingArmies = fromRegionArmies;
            var defendersDestroyed = attackingArmies * .6;
            var attackersDestroyed = defendingArmies * .7;

            var attackingArmiesLeft = attackingArmies - attackersDestroyed;
            var defendingArmiesLeft = defendingArmies - defendersDestroyed;
            if (attackingArmiesLeft <= 0)
                return BattleRegions.Defender;

            if (defendingArmiesLeft <= 0)
                return BattleRegions.Attacker;

            if ((int)attackingArmiesLeft - 1 == toRegionArmies && (int)defendingArmiesLeft + currentState.StartingArmies == fromRegionArmies)
                return BattleRegions.Neither;

            return WhoWinsOnDefend(currentState, (int)defendingArmiesLeft + currentState.StartingArmies, (int)attackingArmiesLeft - 1, opponentIncome, false);
        }

        private int _lastOpposingRegionCount;
        private bool GuessOpponentPlacement(BotState gameState, int opponentIncome)
        {
            if (_thisIsOpponent || gameState.RoundNumber == 1) return false;
            var opponent = gameState.Opponent;
            var opposingRegions = opponent.Regions.Where(r => r.NeighboringEnemies(gameState.MyPlayerName).Any()).ToList();
            var myRegionsWithOpponents = gameState.MyRegions.Where(my => my.NeighboringEnemies(opponent.PlayerName).Count > 0).ToList();
            var lastRound = gameState.RoundNumber - 1;

            if (opponent.PlaceArmyMovesForRound[lastRound].Count == 0)
            {
                if(gameState.VisibleMap.SuperRegions.Any(sr => sr.OwnedByPlayer() == gameState.MyPlayerName))
                {
                    if(opposingRegions.Count == 1 && myRegionsWithOpponents.Count == 1)
                    {
                        opposingRegions.First().Armies += opponent.EstimatedIncome;
                        return true;
                    }
                }
                //don't think I can predict this
                return false;
            }
            if (opponent.OwnedSuperRegions.Any())
            {
                foreach (var ownedSuper in opponent.OwnedSuperRegions.OrderByDescending(sr => sr.Ranking))
                {
                    foreach (var opponentRegion in opponent.Regions.Where(o => o.SuperRegion.Id == ownedSuper.Id && o.StrategicPosition).OrderBy(o => o.Armies))
                    {
                        if (gameState.MyRegions.Any(my => my.IsNeighbor(opponentRegion)))
                        {
                            opponentRegion.Armies += opponent.EstimatedIncome;
                            return true;
                        }
                    }
                }
            }
            //did opponent place all armies in one location last turn?
            if (opponent.PlaceArmyMovesForRound[lastRound].Count == 1)
            {
                //TODO: update this to lastRound <= 2 right before finals
                if (lastRound == 2)
                {
                    var placement = gameState.VisibleMap.GetRegion(opponent.PlaceArmyMovesForRound[lastRound].First().Key.Id);
                    placement.Armies += opponentIncome;
                    //starting with the next super region region by super region ranking that the opponent isn't placing armies in, 
                    //remove neighbors that aren't in the same super
                    foreach (var myRegion in gameState.MyRegions.Where(p => !p.IsNeighbor(placement)))
                    {
                        var neighbors = myRegion.Neighbors.Where(n => n.SuperRegion != myRegion.SuperRegion).ToList();
                        foreach(var neighbor in neighbors)
                            myRegion.Neighbors.Remove(neighbor);
                    }
                    return true;
                }
                if (lastRound != 1)
                {
                    //let's guess he's going to do it again if he still controls that region?
                    //unless we have new armies to worry about
                    if (opponent.PlaceArmyMovesForRound[lastRound - 1].Count == 1)
                    {
                        if (opposingRegions.Count > _lastOpposingRegionCount)
                        {
                            if (gameState.RoundNumber < 10)
                                return false;
                        }
                        var lastRoundMove = opponent.PlaceArmyMovesForRound[lastRound].First();
                        var previousRoundMove = opponent.PlaceArmyMovesForRound[lastRound - 1].First();
                        if (lastRoundMove.Key.Id == previousRoundMove.Key.Id)
                        {
                            if (opponent.AttackTransferMovesForRound[lastRound].Any(attack => attack.FromRegion.Id == lastRoundMove.Key.Id))
                            {
                                var moves = opponent.AttackTransferMovesForRound[lastRound].Where(attack => attack.FromRegion.Id == lastRoundMove.Key.Id);
                                foreach (var move in moves)
                                {
                                    if (move.ToRegion.PlayerName == opponent.PlayerName)
                                    {
                                        //stack, stack, attack. If I still see it, he's probably chasing me
                                        move.ToRegion.Armies += opponent.EstimatedIncome;
                                        return true;
                                    }
                                }
                            }
                            var placedArmyRegion = opponent.PlaceArmyMovesForRound[lastRound].First().Key;
                            if (placedArmyRegion.PlayerName == opponent.PlayerName)
                            {
                                placedArmyRegion.Armies += opponentIncome;
                                return true;
                            }
                        }
                    }
                }
            }

            if (opposingRegions.Count == 1)
            {
                opposingRegions.First().Armies += opponentIncome;
                return true;
            }
            if (myRegionsWithOpponents.Count == 1)
            {
                var myRegion = myRegionsWithOpponents.First();
                var opponentRegion = myRegion.NeighboringEnemies(gameState.Opponent.PlayerName).First(n => n.Armies == myRegion.NeighboringEnemies(gameState.Opponent.PlayerName).Max(o => o.Armies));
                opponentRegion.Armies += opponentIncome;
                return true;
            }
            //TODO: this is an arbitrary number for now. Need to think of a way to figure out how long I want to do this for
            //      Maybe break the same time I do for PlaceAggressiveArmies? After my #1 is captured?
            if (gameState.RoundNumber < 5)
            {
                //see if the opponent split their armies.
                if (opponent.PlaceArmyMovesForRound[lastRound].Count == 2)
                {
                    //what would I do next?
                    var opponentState = new BotState(gameState.RoundNumber);
                    opponentState.UpdateSettings(Constants.YOUR_BOT, opponent.PlayerName);
                    opponentState.UpdateSettings(Constants.STARTING_ARMIES, opponentIncome.ToString(CultureInfo.InvariantCulture));
                    opponentState.UpdateSettings(Constants.OPPONENT_BOT, gameState.MyPlayerName);
                    opponentState.PreferredStartingRegions = gameState.PreferredStartingRegions;
                    foreach (var value in gameState.SetupMapString)
                    {
                        opponentState.SetupMap(value.Value);
                    }
                    opponentState.UpdateMap(gameState.LastMapUpdate);
                    var opponentBot = new Trogabot(true);
                    var results = opponentBot.GetPlaceArmiesMoves(opponentState, 0);
                    foreach (var item in results)
                    {
                        opponent.Regions.First(r => r.Id == item.Region.Id).Armies += item.Armies;
                    }
                    return true;
                }
            }
            if (gameState.MyRegions.Any(are => are.StrategicDefenseRegion))
            {
                var highPriority = gameState.MyRegions.Where(my => my.StrategicDefensePriority == 1).ToList();
                if (!highPriority.Any())
                    highPriority = gameState.MyRegions.Where(my => my.StrategicDefensePriority == 2).ToList();

                var biggestThreat = highPriority.First().NeighboringEnemies(opponent.PlayerName).First();
                foreach (var region in highPriority)
                {
                    foreach (var neighbor in region.NeighboringEnemies(opponent.PlayerName))
                        if (neighbor.Armies > biggestThreat.Armies)
                            biggestThreat = neighbor;
                }
                biggestThreat.Armies += opponentIncome;
                return true;
            }
            return false;
        }

        private void CheckForLargeStack(BotState gameState)
        {
            foreach (var region in gameState.MyRegions.OrderByDescending(my => my.Armies))
            {
                if (!region.NeighboringEnemies(gameState.Opponent.PlayerName).Any())
                {
                    foreach (var opponentRegion in gameState.VisibleMap.Regions.Where(ur => ur.NeighboringEnemies(gameState.Opponent.PlayerName).Count > 0))
                    {
                        opponentRegion.MovesToOpponent = 0;
                        IncrementNeighbors(gameState, opponentRegion.Id, 1);
                    }

                    if (region.MovesToOpponent > 0 && region.Neighbors.All(n => n.OwnedByPlayer(gameState.MyPlayerName)))
                    {
                        if (region.Armies - region.MovesToOpponent > region.MovesToOpponent * 3)
                        {
                            var nextRegion = region.Neighbors.FirstOrDefault(n => n.MovesToOpponent < region.MovesToOpponent);
                            if (nextRegion != null)
                            {
                                _myTurnMoves.LastAttackMoves.Add(new AttackTransferMove(gameState.MyPlayerName, region, nextRegion, region.Armies - 1));
                                region.Armies = 1;
                            }
                        }
                    }
                }
            }
        }

        public static void Main(string[] args)
        {
            var parser = new BotParser(new Trogabot());
            parser.Run();
        }
    }
}