using System.Collections.Generic;
using WarLightStarterBot.Main;
using WarLightStarterBot.Moves;

namespace WarLightStarterBot.Bot
{

    public interface IBot
    {
        List<Region> GetPreferredStartingRegions(BotState state, long timeOut);
        List<PlaceArmiesMove> GetPlaceArmiesMoves(BotState gameState, long timeOut);
        List<AttackTransferMove> GetAttackTransferMoves(BotState state, long timeOut);
    }

}