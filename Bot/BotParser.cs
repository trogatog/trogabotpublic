using System;
using System.IO;
using System.Linq;
using System.Text;
using WarLightStarterBot.ServerCommands;

namespace WarLightStarterBot.Bot
{
    internal class BotParser
    {
        private readonly Trogabot _bot;

        private readonly BotState _currentState;

        public BotParser(Trogabot bot)
        {
            _bot = bot;
            _currentState = new BotState();
        }

        public void Run()
        {
            //var stream = new StreamReader("output.txt");
            while (true)
            {
                //var line = stream.ReadLine();

                var line = Console.ReadLine();
                if (line == null)
                    break;
                line = line.Trim();

                if (line.Length == 0)
                    continue;

                var parts = line.Split(' ');
                var command = parts[0];
                var output = new StringBuilder();
                switch (command)
                {
                    case Constants.PICK_STARTING_REGIONS:
                        // Pick which regions you want to start with
                        _currentState.SetPickableStartingRegions(parts.ToList());
                        _currentState.PreferredStartingRegions = _bot.GetPreferredStartingRegions(_currentState, long.Parse(parts[1]));
                        foreach (var region in _currentState.PreferredStartingRegions)
                            output.Append(region.Id + " ");

                        Console.WriteLine(output.ToString());
                        break;

                    case Constants.GO:
                        var subcommand = parts[1];
                        switch (subcommand)
                        {
                            case Constants.PLACE_ARMIES:
                                // Place armies
                                var placeArmiesMoves = _bot.GetPlaceArmiesMoves(_currentState, long.Parse(parts[2]));
                                foreach (var move in placeArmiesMoves)
                                    output.Append(move.String + Constants.LINE_SPLIT);
                                break;
                            case Constants.ATTACK_OR_TRANSFER:
                                var attackTransferMoves = _bot.GetAttackTransferMoves(_currentState, long.Parse(parts[2]));
                                foreach (var move in attackTransferMoves)
                                    output.Append(move.String + Constants.LINE_SPLIT);
                                break;
                        }
                        Console.WriteLine(output.Length > 0 ? output.ToString() : Constants.NO_MOVES);
                        break;

                    case Constants.SETTINGS:
                        _currentState.UpdateSettings(parts[1], parts[2]);
                        break;

                    case Constants.SETUP_MAP:
                        _currentState.SetupMap(parts);
                        break;

                    case Constants.UPDATE_MAP:
                        _currentState.UpdateMap(parts);
                        break;

                    case Constants.OPPONENT_MOVES:
                        _currentState.ReadOpponentMoves(parts);
                        break;

                    default:
                        Console.Error.WriteLine(FormattingConstants.ERROR_FORMAT, "line:", line);
                        break;
                }
            }
        }

    }

}