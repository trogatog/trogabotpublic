using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using WarLightStarterBot.Enums;
using WarLightStarterBot.Extensions;
using WarLightStarterBot.Main;
using WarLightStarterBot.Moves;
using WarLightStarterBot.ServerCommands;

namespace WarLightStarterBot.Bot
{
    public class BotState
    {
        public BotState(int roundNumber = 0)
        {
            MyPlayerName = string.Empty;
            FullMap = new Map();
            PickableStartingRegions = new List<Region>();
            RoundNumber = roundNumber; //for mocking opponent
            SetupMapString = new Dictionary<string, string[]>();
        }

        public string MyPlayerName { get; private set; }
        public int StartingArmies { get; private set; }
        public int RoundNumber { get; private set; }
        public Map VisibleMap { get; private set; }
        public Map FullMap { get; private set; }
        public List<Region> PickableStartingRegions { get; set; }
        public string[] LastMapUpdate { get; private set; }
        public Dictionary<string,string[]> SetupMapString { get; private set; }
        
        public void UpdateSettings(string key, string value)
        {
            switch (key)
            {
                case Constants.YOUR_BOT:
                    MyPlayerName = value;
                    break;
                case Constants.OPPONENT_BOT:
                    Opponent = new OpponentInfo(value);
                    break;
                case Constants.STARTING_ARMIES:
                    StartingArmies = int.Parse(value);
                    break;
            }
        }

        // Initial map is given to the bot with all the information except for player and armies info
        public void SetupMap(string[] mapInput)
        {
            int i;
            int superRegionId;
            switch (mapInput[1])
            {
                case Constants.SUPER_REGIONS:
                    SetupMapString.Add(Constants.SUPER_REGIONS, mapInput);
                    for (i = 2; i < mapInput.Length; i++)
                    {
                        try
                        {
                            superRegionId = int.Parse(mapInput[i]);
                            i++;
                            int reward = int.Parse(mapInput[i]);
                            FullMap.Add(new SuperRegion(superRegionId, reward));
                        }
                        catch (Exception e)
                        {
                            Console.Error.WriteLine(FormattingConstants.ERROR_FORMAT, Constants.SUPER_REGIONS, e);
                        }
                    }
                    break;
                case Constants.REGIONS:
                    SetupMapString.Add(Constants.REGIONS, mapInput);
                    for (i = 2; i < mapInput.Length; i++)
                    {
                        try
                        {
                            var regionId = int.Parse(mapInput[i++]);
                            superRegionId = int.Parse(mapInput[i]);
                            var superRegion = FullMap.GetSuperRegion(superRegionId);
                            FullMap.Add(new Region(regionId, superRegion));
                        }
                        catch (Exception e)
                        {
                            Console.Error.WriteLine(FormattingConstants.ERROR_FORMAT, Constants.REGIONS, e);
                        }
                    }
                    break;
                case Constants.NEIGHBORS:
                    SetupMapString.Add(Constants.NEIGHBORS, mapInput    );
                    for (i = 2; i < mapInput.Length; i++)
                    {
                        try
                        {
                            var region = FullMap.GetRegion(int.Parse(mapInput[i++]));
                            var neighborIds = mapInput[i].Split(Constants.LINE_SPLIT);
                            foreach (var neighbor in neighborIds.Select(t => FullMap.GetRegion(int.Parse(t))))
                            {
                                region.AddNeighbor(neighbor);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.Error.WriteLine(FormattingConstants.ERROR_FORMAT, Constants.NEIGHBORS, e);
                        }
                    }
                    break;
            }
        }

        // Regions from wich a player is able to pick his preferred starting regions
        public void SetPickableStartingRegions(List<String> mapInput)
        {
            for (var i = 2; i < mapInput.Count; i++)
            {
                try
                {
                    var regionId = int.Parse(mapInput[i]);
                    var pickableRegion = FullMap.GetRegion(regionId);
                    PickableStartingRegions.Add(pickableRegion);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(FormattingConstants.ERROR_FORMAT, Constants.PICKABLE_REGIONS, e);
                }
            }
        }

        private void UpdateRankings()
        {
            var currentSupers = MyRegions.Select(my => my.SuperRegion).Distinct().ToList();

            if (currentSupers.Count == 3 && currentSupers.All(cs => cs.OwnedByPlayer() != MyPlayerName))
            {
                if (currentSupers.Any(su => su.Id == (int) SuperRegionNames.Australia))
                {
                    if (MyRegions.First(my => my.SuperRegion.Id == (int) SuperRegionNames.Australia).NeighboringEnemies(Opponent.PlayerName).Any())
                    {
                        VisibleMap.SuperRegions.First(s => s.Id == (int) SuperRegionNames.Australia).Ranking = 1;
                        FullMap.GetSuperRegion((int) SuperRegionNames.Australia).Ranking = 1;
                        VisibleMap.SuperRegions.First(s => s.Id == (int) SuperRegionNames.SouthAmerica).Ranking = 2;
                        FullMap.GetSuperRegion((int) SuperRegionNames.SouthAmerica).Ranking = 2;
                        VisibleMap.SuperRegions.First(s => s.Id == (int)SuperRegionNames.Africa).Ranking = 3;
                        FullMap.GetSuperRegion((int)SuperRegionNames.Africa).Ranking = 3;
                    }
                }
                if (currentSupers.Any(su => su.Id == (int) SuperRegionNames.SouthAmerica))
                {
                    if (!MyRegions.First(my => my.SuperRegion.Id == (int) SuperRegionNames.SouthAmerica).NeighboringEnemies(Opponent.PlayerName).Any())
                    {
                        if (currentSupers.Any(su => su.Id == (int) SuperRegionNames.Africa))
                        {
                            if (MyRegions.First(my => my.SuperRegion.Id == (int) SuperRegionNames.Africa).NeighboringEnemies(Opponent.PlayerName).Any())
                            {
                                var saRanking = FullMap.GetSuperRegion((int) SuperRegionNames.SouthAmerica).Ranking;
                                var aRanking = FullMap.GetSuperRegion((int) SuperRegionNames.Africa).Ranking;
                                VisibleMap.SuperRegions.First(s => s.Id == (int) SuperRegionNames.Africa).Ranking = saRanking;
                                FullMap.GetSuperRegion((int) SuperRegionNames.Africa).Ranking = saRanking;
                                VisibleMap.SuperRegions.First(s => s.Id == (int) SuperRegionNames.SouthAmerica).Ranking = aRanking;
                                FullMap.GetSuperRegion((int) SuperRegionNames.SouthAmerica).Ranking = aRanking;
                            }
                        }
                    }
                }
                return;
            }
            
            foreach (var super in FullMap.SuperRegions)
            {
                super.Ranking = ((SuperRegionNames) super.Id).GetAttribute<RankingAttribute>().Rank;
            }
            var australia = VisibleMap.SuperRegions.First(s => s.Id == (int)SuperRegionNames.Australia);
            if (VisibleMap.GetSuperRegion((int)SuperRegionNames.Australia).OwnedByPlayer() == MyPlayerName)
            {
                australia.Ranking = 1;
                FullMap.GetSuperRegion(australia.Id).Ranking = 1;
                var southAmerica = VisibleMap.SuperRegions.FirstOrDefault(s => s.Id == (int)SuperRegionNames.SouthAmerica);
                var africa = VisibleMap.SuperRegions.FirstOrDefault(s => s.Id == (int)SuperRegionNames.Africa);
                if (southAmerica != null)
                    southAmerica.Ranking = 3;
                FullMap.GetSuperRegion((int)SuperRegionNames.SouthAmerica).Ranking = 3;
                if (africa != null)
                    africa.Ranking = 2;
                FullMap.GetSuperRegion((int)SuperRegionNames.Africa).Ranking = 2;
            }
        }

        // Visible regions are given to the bot with player and armies info
        public void UpdateMap(string[] mapInput)
        {
            LastMapUpdate = mapInput;
            VisibleMap = FullMap.GetMapCopy();
            for (var i = 1; i < mapInput.Length; i++)
            {
                try
                {
                    var region = VisibleMap.GetRegion(int.Parse(mapInput[i]));
                    var playerName = mapInput[++i];
                    var armies = int.Parse(mapInput[++i]);

                    region.PlayerName = playerName;
                    region.Armies = armies;
                    region.StartingArmies = armies;
                    region.StackHere = false;
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(FormattingConstants.ERROR_FORMAT, Constants.UPDATE_MAP, e);
                }
            }
            UnknownRegions = VisibleMap.Regions.Where(region => region.PlayerName == Constants.UNKNOWN).ToList();

            // Remove regions which are unknown.
            foreach (var unknownRegion in UnknownRegions)
            {
                unknownRegion.MovesToOpponent = int.MaxValue;
            }
            
            MyRegions = VisibleMap.Regions.Where(r => r.OwnedByPlayer(MyPlayerName)).ToList();
            NeutralRegions = VisibleMap.Regions.Where(r => r.OwnedByPlayer(Constants.NEUTRAL_PLAYER)).ToList();
            Opponent.Regions = VisibleMap.Regions.Where(r => r.OwnedByPlayer(Opponent.PlayerName)).ToList();
            
            UpdateRankings();
            
            foreach (var neutralRegion in NeutralRegions)
            {
                neutralRegion.MovesToOpponent = int.MaxValue;
                neutralRegion.HasAttacker = false;
            }

            foreach (var region in MyRegions)
            {
                region.MovesToOpponent = int.MaxValue;
                region.StrategicDefenseRegion = false;
                region.StrategicDefensePriority = byte.MaxValue;
                
                if (region.Neighbors.All(n => n.OwnedByPlayer(MyPlayerName)) && region.Armies == 1)
                {
                    if (!region.Neighbors.Any(n => n.NeighboringEnemies(Opponent.PlayerName).Count > 0))
                    {
                        if (region.Neighbors.Any(n => n.Armies > 1))
                        {
                            var stackedNeighbors = region.Neighbors.Where(n => n.Armies > 1);
                            if (stackedNeighbors.Any(n => n.SubRegionRanking > region.SubRegionRanking))
                                continue;
                            
                        }
                        region.MakeMeInvisible();
                    }
                }
            }

            foreach (var region in MyRegions.Where(my => my.NeighboringEnemies(Opponent.PlayerName).Any()))
            {
                region.MovesToOpponent = 0;
                IncrementNeighbors(region, 1);
            }

            foreach (var superRegion in VisibleMap.SuperRegions)
            {
                superRegion.SetToBeConquered = false;
                superRegion.KeepVisible = false;
                superRegion.SetArmiesNeededToConquer();
            }

            foreach (var superRegion in VisibleMap.SuperRegions.Where(sr => sr.OwnedByPlayer() == MyPlayerName))
            {
                foreach (var subRegion in superRegion.SubRegions.Where(sr => sr.Neighbors.Any(n => n.SuperRegion != superRegion)))
                {
                    //if there's any enemies homing in on the super region, mark this territory as a defensive position until we can take more
                    if (subRegion.NeighboringEnemies(Opponent.PlayerName).Any())
                    {
                        subRegion.StrategicDefenseRegion = true;
                        subRegion.StrategicDefensePriority = 1;
                    }
                    //if there's no enemies directly next to it, check if we own a territory next to it with a frontier, and mark that as defensive
                    else
                    {
                        foreach (var neighbor in subRegion.Neighbors.Where(n => n.SuperRegion != superRegion))
                        {
                            if (neighbor.NeighboringEnemies(Opponent.PlayerName).Any())
                            {
                                neighbor.StrategicDefenseRegion = true;
                                neighbor.StrategicDefensePriority = 2;
                            }
                        }
                    }
                }
            }

            Opponent.UpdateMapInfo(this);
            UpdateSuperStatus();
        }

        private void IncrementNeighbors(Region region, int moveNumber)
        {
            foreach (var neighbor in region.FriendlyNeighbors().Where(fn => fn.MovesToOpponent > moveNumber))
            {
                neighbor.MovesToOpponent = moveNumber;
                IncrementNeighbors(neighbor, moveNumber + 1);
            }
        }

        // Parses a list of the opponent's moves every round. 
        // Clears it at the start, so only the moves of this round are stored.
        public void ReadOpponentMoves(string[] moveInput)
        {
            var opponentPlaceArmies = new List<PlaceArmiesMove>();
            var opponentAttackTransfer = new List<AttackTransferMove>();
            for (var i = 1; i < moveInput.Length; i++)
            {
                try
                {
                    switch (moveInput[i + 1])
                    {
                        case Constants.PLACE_ARMIES:
                        {
                            var region = VisibleMap.GetRegion(int.Parse(moveInput[i + 2]));
                            var playerName = moveInput[i];
                            var armies = int.Parse(moveInput[i + 3]);
                            opponentPlaceArmies.Add(new PlaceArmiesMove(playerName, region, armies));
                            i += 3;
                        }
                            break;
                        case Constants.ATTACK_OR_TRANSFER:
                        {
                            var fromRegion = VisibleMap.GetRegion(int.Parse(moveInput[i + 2])) ??
                                             FullMap.GetRegion(int.Parse(moveInput[i + 2]));

                            var toRegion = VisibleMap.GetRegion(int.Parse(moveInput[i + 3])) ??
                                           FullMap.GetRegion(int.Parse(moveInput[i + 3]));

                            var playerName = moveInput[i];
                            var armies = int.Parse(moveInput[i + 4]);
                            opponentAttackTransfer.Add(new AttackTransferMove(playerName, fromRegion, toRegion, armies));
                            i += 4;
                        }
                            break;
                        default:
                            continue;
                    }
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(FormattingConstants.ERROR_FORMAT, Constants.OPPONENT_MOVES, e);
                }
            }
            Opponent.SetPlayerMovesForRound(this, opponentPlaceArmies, opponentAttackTransfer);
            RoundNumber++;
        }

        private void UpdateSuperStatus()
        {
            var knownSuperStatus = MyRegions.Union(NeutralRegions);
            var worldMap = FullMap.GetMapCopy();

            var occupiedSupers = knownSuperStatus.Select(s => s.SuperRegion.Id).Distinct().ToList();
            foreach (var superId in occupiedSupers)
            {
                var remove = worldMap.SuperRegions.First(sr => sr.Id == superId);
                worldMap.SuperRegions.Remove(remove);
            }
            UnknownStatusSuper = worldMap.SuperRegions;
        }

        public List<Region> PreferredStartingRegions { get; set; } 
        public List<Region> MyRegions { get; private set; }
        public List<Region> NeutralRegions { get; private set; }
        public List<Region> UnknownRegions { get; private set; } 
        public List<SuperRegion> UnknownStatusSuper { get; private set; }
        public OpponentInfo Opponent { get; private set; }
    }

}