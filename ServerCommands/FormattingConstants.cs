﻿namespace WarLightStarterBot.ServerCommands
{
    public class FormattingConstants
    {
        public const string ERROR_FORMAT = "Unable to parse {0} {1}";
        public const string ADD_ERROR_FORMAT = "{0} cannot be Added: id already exists.";
    }
}
