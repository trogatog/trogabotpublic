﻿namespace WarLightStarterBot.ServerCommands
{
    public static class Constants
    {
        public const char LINE_SPLIT = ',';
        public const string YOUR_BOT = "your_bot";
        public const string OPPONENT_BOT = "opponent_bot";
        public const string UNKNOWN = "unknown";
        public const string STARTING_ARMIES = "starting_armies";
        public const string PICK_STARTING_REGIONS = "pick_starting_regions";
        public const string PLACE_ARMIES = "place_armies";
        public const string ATTACK_OR_TRANSFER = "attack/transfer";
        public const string SETTINGS = "settings";
        public const string SETUP_MAP = "setup_map";
        public const string UPDATE_MAP = "update_map";
        public const string OPPONENT_MOVES = "opponent_moves";
        public const string GO = "go";
        public const string NO_MOVES = "No moves";
        public const string SUPER_REGIONS = "super_regions";
        public const string REGIONS = "regions";
        public const string PICKABLE_REGIONS = "pickable_regions";
        public const string NEIGHBORS = "neighbors";
        public const string NEUTRAL_PLAYER = "neutral";
    }
}
